<?php

/**
* 
*/
class Student extends CI_controller
{
	public function __construct() 
	{

	parent::__construct();

	// Load form helper library
	$this->load->helper(array('form', 'url'));

	// Load form validation library
	$this->load->library('form_validation');

	// Load session library
	$this->load->library('session');

	// Load database
	$this->load->model('model_users');

	}


# index --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function index()
	{

		$this->load->view('login');
	}

# login --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function login()
	{
		$this->load->view('login');
	}

# sign-up removed --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function signup()
	{
		$this->load->view('signup');
	}

# dashboard --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function studentinfo()
	{
			$this->load->model('Model_users');
			$this->input->post('$student_id');
			$this->load->view('studentinfo');

	}

# curriculum --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function curriculum()
	{
		if ($this->session->userdata('logged_in')) {
			
			$this->load->model('Model_users');
			
			$this->input->post('$student_id');
			$username=$this->input->post('username');

			//$data['all_sub'] = $this->Model_users->get_allsubjects($username);
			$this->load->view('curriculum');
		}
		else
		{
			redirect('student');
		}
		
	}

# csv out for subjects --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function download_student_sub()
	{
		if ($this->session->userdata('logged_in')) 
		{
			$student_id =$this->input->get('student_id');
			$this->load->model('Model_users');
			$this->Model_users->download_sub($student_id);
		}
		else
		{
			redirect('student');
		}

				
	
	}

# login validation -->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function login_validation()
	{

			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
			$this->load->view('studentinfo');
			}else{
			$this->load->view('login');
			}
			} else {
			$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
			);
			$result = $this->model_users->loggedin_student($data);
			if ($result == TRUE) {

			$username = $this->input->post('username');
			$result = $this->model_users->view_det($username);
			if ($result != false) {
			$session_data = array(
			'username' => $result[0]->student_id,
			'password' => $result[0]->student_password,
			'student_status' => $result[0]->student_status,
			'sur_name' => $result[0]->sur_name,
			'first_name' => $result[0]->first_name,
			'middle_name' => $result[0]->middle_name,
			'gender' => $result[0]->gender,
			'age' => $result[0]->age,
			'date_of_birth' => $result[0]->date_of_birth,
			'status' => $result[0]->status,
			'citizenship' => $result[0]->citizenship,
			'home_address' => $result[0]->home_address,
			'tel_no' => $result[0]->tel_no,
			'cell_no' => $result[0]->cell_no,
			'email_address' => $result[0]->email_address,
			'course' => $result[0]->course,
			'major_in' => $result[0]->major_in,
			'school_year' => $result[0]->school_year,
			'year_level' => $result[0]->year_level,
			'semester' => $result[0]->semester,

			);
			
			$this->session->set_userdata('logged_in', $session_data);
			
			redirect('student/studentinfo');
			}
			} else {
			
			redirect('student');
			}
		}
			      
	}

# logut --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('student/login');
	}

# change password --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function change_password()
	{
		$username=$this->input->post('username');
		$old_pass=$this->input->post('old_pass');
		
		$this->load->model('Model_users');
		$data = $this->Model_users->search_to_change($username,$old_pass);
		if ($data->num_rows() == true) {
			
			$username=$this->input->post('username');
			$old_pass=$this->input->post('old_pass');
			$new_pass=$this->input->post('new_pass');
			$this->load->model('Model_users');
			$pass_chang = $this->Model_users->change_password($username,$old_pass,$new_pass);
			
			$this->session->sess_destroy();
			$this->load->view('password_changed');
		
		}
		else
		{

			$this->load->view('studentinfo');
		}
	}

# password change --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function password_changed()
	{
		$this->load->view('password_changed');
	}

# pdf file not functioning --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function pdf_download()
	{
		$this->load->view('pdf_download');
	}
	public function download_pdf()
	{
		$student_id = $this->input->get('student_id');
		$this->load->library('m_pdf');
		$this->load->model('Model_users');
		$data['firstyear_first'] = $this->Model_users->firstyear_first($student_id);
		$data['firstyear_second'] = $this->Model_users->firstyear_second($student_id);
		$data['secondyear_first'] = $this->Model_users->secondyear_first($student_id);
		$data['secondyear_second'] = $this->Model_users->secondyear_second($student_id);
		$data['thirdyear_first'] = $this->Model_users->thirdyear_first($student_id);
		$data['thirdyear_second'] = $this->Model_users->thirdyear_second($student_id);
		$data['fourthyear_first'] = $this->Model_users->fourthyear_first($student_id);
		$data['fourthyear_second'] = $this->Model_users->fourthyear_second($student_id);
		$pdfFilePath = "Subject List.pdf";
		$this->m_pdf->pdf->writeHTML($this->load->view('pdf_download',$data,true));
		$this->m_pdf->pdf->Output($pdfFilePath,"D");


	}

# upload profile picture --->
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public function do_upload()
        {
        
			    $config['upload_path']          = './upload';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 400;
                $config['max_width']            = 2000;
                $config['max_height']           = 2000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $this->load->model('Model_users');
                        
                        $this->load->view('studentinfo', $error);
                }
                else
                {
                    $student_id = $this->input->post('student_id');
                    $this->load->model('Model_users');
             	  	$search_pic = $this->Model_users->search_picture($student_id);
                      	if ($search_pic->num_rows() == true) 
                     	{
                     	        $this->Model_users->update_images($this->upload->data(),$student_id);        
                    	}
                     	 else
                    	{
                   		    	$this->Model_users->insert_images($this->upload->data(),$student_id);       
                        }
                        $this->load->model('Model_users');
                        redirect('student/studentinfo');
           		}
            }

            public function concern()
            {
            	$student_id = $this->input->post('student_id');
            	$this->load->model('Model_users');
            	$studentid_found = $this->Model_users->find_id($student_id);

            	if ($studentid_found->num_rows() == true) {
            		
            		$student_id = $this->input->post('student_id');
            		$email_address = $this->input->post('email_address');
            		$student_concern = $this->input->post('student_concern');

            		$this->load->model('Model_users');
            		$sent = $this->Model_users->sent_request($student_id,$email_address,$student_concern);
            		redirect('student');
            	}
            	else
            	{
            		redirect('student');
            	}
            }



}
?>