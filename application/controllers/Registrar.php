<?php

/**
* 
*/
class Registrar extends CI_controller
{
		public function __construct()
		{
			parent::__construct();

			// Load form helper library
			$this->load->helper('form');

			// Load form validation library
			$this->load->library('form_validation');

			// Load session library
			$this->load->library('session');

			// Load database
			$this->load->model('model_users');
		}

	
	public function login(){
		$this->load->view('registrar/login');
	}
	public function index(){
		$this->load->view('registrar/login');
	}
	public function update_subject()
	{
		if ($this->session->userdata('is_logged_in')) {
			$stud_grades_no = $this->input->get('stud_grades_no');
		
			$this->load->model('Model_users');

			#$data['student_id'] = $this->Model_users->get_student_id($stud_grades_no);
			$data['subject_edit'] = $this->Model_users->get_subject($stud_grades_no);



			$this->load->view('registrar/option/update_subject',$data);
		}
		else
		{
			redirect('regsitrar');
		}
		

	}

	public function change_password()
	{
		$registrar_id=$this->input->post('registrar_id');
		$old_pass=$this->input->post('old_pass');
		
		$this->load->model('Model_users');
		$data = $this->Model_users->search_to_change_reg($registrar_id,$old_pass);
		if ($data->num_rows() == true) {
			
			$registrar_id=$this->input->post('registrar_id');
			$old_pass=$this->input->post('old_pass');
			$new_pass=$this->input->post('new_pass');
			$this->load->model('Model_users');
			$pass_chang = $this->Model_users->change_password_reg($registrar_id,$old_pass,$new_pass);
			
			$this->session->sess_destroy();
			redirect('registrar');
		
		}
		else
		{

			redirect('registrar/studentlist');
		}
	}

	public function generate_csv()
	{
		$this->load->model('Model_users');
		$this->Model_users->download_file();
	}

	public function your_account()
	{
		$this->load->view('your_account');
	}

	public function full_details()
	{
		if ($this->session->userdata('is_logged_in')) {
			$student_id = $this->input->get('student_id');
			$this->load->model('Model_users');
			$data['student_data'] = $this->Model_users->get_student_data($student_id);

			$this->load->view('registrar/option/full_details',$data);
		}else{
			redirect('registrar/login');
		}
		
	}
	//public function studentnotif()
	//{	
	//	if ($this->session->userdata('is_logged_in')) {
	//		$this->load->view('registrar/registrar_studentnotif');
	//	}else{
	//		redirect('registrar/registrar_login');
	//	}
		
	//}
	public function addstudent()
	{
		if ($this->session->userdata('is_logged_in')) {
			$this->load->view('registrar/addstudent');
		}else{
			redirect('registrar/login');
		}
		
	}
	public function update_students()
	{
		if ($this->session->userdata('is_logged_in')) {

			$student_id = $this->input->get('student_id');
			$this->load->model('Model_users');
			$data['student_data'] = $this->Model_users->get_student_data($student_id);
			$data['student_sub'] = $this->Model_users->get_student_sub($student_id);
			$this->load->view('registrar/option/update_students',$data);
		}else{
			redirect('registrar/login');
		}
		

	}
	public function update_student_info()
	{
		$student_id=$this->input->post('student_id');
		$student_pass=$this->input->post('student_pass');
		$opt_not=$this->input->post('opt_not');
		$sname=$this->input->post('sname');
		$fname=$this->input->post('fname');
		$mname=$this->input->post('mname');
		$gender=$this->input->post('gender');
		$age=$this->input->post('age');
		$date= $this->input->post('date');

		$status=$this->input->post('status');
		$citizenship=$this->input->post('citizenship');
		$haddress=$this->input->post('haddress');
		$tnumber=$this->input->post('tnumber');
		$cnumber=$this->input->post('cnumber');
		$eaddress=$this->input->post('eaddress');
		$course=$this->input->post('course');
		$major_in=$this->input->post('major_in');
		$school_year=$this->input->post('school_year');
		$year_level=$this->input->post('year_level');
		$semester=$this->input->post('semester');

		$this->load->model('Model_users');
		$this->Model_users->update_this($student_id,$student_pass,$opt_not,$sname,$fname,$mname,$gender,$age,$date,$status,$citizenship,$haddress,$tnumber,$cnumber,$eaddress,$course,$major_in,$school_year,$year_level,$semester);

		redirect('registrar/studentlist');


	}
	public function update_sub()
	{
		if ($this->session->userdata('is_logged_in')) {
			$stud_grades_no = $this->input->post('stud_grades_no');
			$student_id = $this->input->post('student_id');
			$course = $this->input->post('course');
			$year_level = $this->input->post('year_level');
			$semester = $this->input->post('semester');
			$instructor = $this->input->post('instructor');
			$subject_code = $this->input->post('subject_code');
			$subject_title = $this->input->post('subject_title');
			$no_units = $this->input->post('units');
			$prelim = $this->input->post('prelim');
			$midterm = $this->input->post('midterm');
			$prefinal = $this->input->post('prefinal');
			$final = $this->input->post('final');
			$rating = $this->input->post('rating');


			$this->load->model('Model_users');
			$this->Model_users->sub_update($stud_grades_no,$student_id,$course,$year_level,$semester,$instructor,$subject_code,$subject_title,$no_units,$prelim,$midterm,$prefinal,$final,$rating);
			
			$data['subject_edit'] = $this->Model_users->get_subject($stud_grades_no);
			
			$this->load->view('registrar/option/update_subject',$data);
		}

		else
		{
			redirect('registrar');
		}


	}
	public function update_subModal()
	{
		/*
		$stud_grades_no = $_POST['stud_grades_no'];
		$student_id =  $_POST['student_id'];
		$course =  $_POST['course'];
		$year_level =  $_POST['year_level'];
		$semester =  $_POST['semester'];
		
		$stmt = $this->db->update("UPDATE student_grades SET student_id=? course=? year_level=? semester=? where stud_grades_no=?");*/
		
		

		/*
		$stud_grades_no = $this->input->post('stud_grades_no');
		$student_id = $this->input->post('student_id');
		$course = $this->input->post('course');
		$year_level = $this->input->post('year_level');
		$semester = $this->input->post('semester');*/

		$this->load->model('Model_users');
		$data = $this->Model_users->sub_updateModal($stud_grades_no);
		echo json_encode($data);


	}
	public function drop_subject()
	{
		$student_id = $this->input->get('student_id');
		$stud_grades_no = $this->input->get('stud_grades_no');
		
		$this->load->model('Model_users');

		$this->Model_users->drop_subject($stud_grades_no);
		
		$data['student_data'] = $this->Model_users->get_student_data1($student_id);
		$data['student_sub'] = $this->Model_users->get_student_sub1($student_id);
		$this->load->view('registrar/option/update_students',$data);

		
	}

	public function add_subject()
	{
		if ($this->session->userdata('is_logged_in')) {
			$student_id = $this->input->get('student_id');

			$subjects =$this->db->get_where('student_grades',array('student_id'=>$student_id));
			$data['subjects'] = $subjects;

			$student_id = $this->input->get('student_id');
			$stud_no = $this->db->get_where('student',array('student_id'=>$student_id));
			$data['idto'] = $stud_no;

			$course = $this->input->get('course');
			$this->load->model('Model_users');
			$rec_sub = $this->Model_users->load_sub($course);
			$data['subjects_demo'] = $rec_sub;
			$this->load->view('registrar/option/add_subject',$data);
		}else{
			redirect('registrar/login');
		}
	}
	public function login_validation(){
		

			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['is_logged_in'])){
			$this->load->view('registrar/studentlist');
			}else{
			$this->load->view('registrar/login');
			}
			} else {
			$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
			);
			$result = $this->model_users->loggedin_registrar($data);
			if ($result == TRUE) {

			$username = $this->input->post('username');
			$result = $this->model_users->view_registrar($username);
			if ($result != false) {
			$session_data = array(
			'registrar_id' => $result[0]->registrar_id,
			'lastname' => $result[0]->lastname,
			'firstname' => $result[0]->firstname,
			'middlename' => $result[0]->middlename,
			'gender' => $result[0]->gender,
			'emailaddress' => $result[0]->emailaddress,
			'username' => $result[0]->username,
			'password' => $result[0]->password,

			
			

			
			);
			// Add user data in session
			$this->session->set_userdata('is_logged_in', $session_data);
			redirect('registrar/studentlist');
			}
			} else {
			//$data = array(
			//'error_message' => 'Invalid Username or Password'
		//	);
		//		$this->load->view('registrar/login',$data);
			redirect('registrar/login','$data');
			}
			}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('registrar/login');
	}
	
	public function insert_student()
	{
		// Search if student exist

		$student_id=$this->input->post('student_id');
		$this->load->model('Model_users');
		$query = $this->Model_users->data_exist($student_id);

		if ($query->num_rows() == false)
		{
			$student_id=$this->input->post('student_id');
			$student_pass=$this->input->post('student_pass');
			$opt_not=$this->input->post('opt_not');
			$sname=$this->input->post('sname');
			$fname=$this->input->post('fname');
			$mname=$this->input->post('mname');
			$gender=$this->input->post('gender');
			$age=$this->input->post('age');

			// Insert from 3 input to 1 column
			$month=$this->input->post('month');
			$day=$this->input->post('day');
			$year=$this->input->post('year');
			$date= $month .'-'.$day.'-'.$year;
			// End

			$status=$this->input->post('status');
			$citizenship=$this->input->post('citizenship');
			$haddress=$this->input->post('haddress');
			$tnumber=$this->input->post('tnumber');
			$cnumber=$this->input->post('cnumber');
			$eaddress=$this->input->post('eaddress');
			$course=$this->input->post('course');
			$major_in=$this->input->post('major_in');
			$school_year=$this->input->post('school_year');
			$year_level=$this->input->post('year_level');
			$semester=$this->input->post('semester');

			$this->load->model('Model_users');
			$this->Model_users->create_student($student_id,$student_pass,$opt_not,$sname,$fname,$mname,$gender,$age,$date,$status,$citizenship,$haddress,$tnumber,$cnumber,$eaddress,$course,$major_in,$school_year,$year_level,$semester);
			
			

			// Email Username and Password

			$subject = "Your Username and Password";
			$student_id = $this->input->post('student_id');
			
			$eaddress=$this->input->post('eaddress');

			$this->load->model('Model_users');
			$data['login_info'] = $this->Model_users->login_info($student_id);

			$this->load->library('email');

			$message = $this->load->view('your_account',$data,TRUE);
			        
			      
	        $this->email->from('britzjam@gmail.com'); // change it to yours
			$this->email->to($eaddress);// change it to yours
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->send();
			
			$student_id=$this->input->post('student_id');
			$this->load->model('Model_users');
			$profile_id = $this->Model_users->insert_profile_id($student_id);
			// redirect to dashboard
			redirect('registrar/studentlist');
		}
		else
		{
			
			redirect('registrar/addstudent');
			
		}

	}
	public function studentlist(){
		if ($this->session->userdata('is_logged_in')) {
			$this->load->model('Model_users');
			$students=$this->Model_users->get_students();
			$data['students']=$students;

			
			$this->load->view('registrar/studentlist',$data);
		}else{
			$this->load->view('registrar/login');
		}
		

	}
	
	
	public function view_full()
	{
		//$student_id = $this->input->get('student_id');
		//$this->load->model('Model_users');
		//$data['data']=$this->Model_users->full_student_details($student_id);
		//$this->load->view('full_details',$data);
		//$student_id = $this->input->get('student_id');
		//$this->load->model('Model_users');
		//$data=$this->Model_users->full_student_details('$student_id');
		//$this->load->view('full_details',$data);
	}
	public function insert_subject()
	{

		$student_id = $this->input->post('student_id');
		$subject_code = $this->input->post('subject_code');
		$this->load->model('Model_users');
		$query = $this->Model_users->subject_exist($student_id,$subject_code);

		if ($query->num_rows() == false)
		{
			
			
			$student_id=$this->input->post('student_id');
			$course=$this->input->post('course');
			$year_level=$this->input->post('year_level');
			$semester=$this->input->post('semester');
			$instructor=$this->input->post('instructor');
			$subject_code=$this->input->post('subject_code');
			$subject_title=$this->input->post('subject_title');
			$no_units=$this->input->post('no_units');
			$prelim=$this->input->post('prelim');
			$midterm=$this->input->post('midterm');
			$prefinal=$this->input->post('prefinal');
			$final=$this->input->post('final');
			$rating=$this->input->post('rating');
			
			

			$this->load->model('Model_users');
			$this->Model_users->create_subject($student_id,$course,$year_level,$semester,$instructor,$subject_code,$subject_title,$no_units,$prelim,$midterm,$prefinal,$final,$rating);
			
			

			$student_id=$this->input->post('student_id');
			$subjects =$this->db->get_where('student_grades',array('student_id'=>$student_id));
			$data1['subjects'] = $subjects;

			$student_id = $this->input->post('student_id');
			$stud_no = $this->db->get_where('student',array('student_id'=>$student_id));
			$data2['idto'] = $stud_no;

			$course=$this->input->post('course');
			$rec_sub = $this->Model_users->load_sub($course);
			$data3['subjects_demo'] = $rec_sub;
			$this->load->view('registrar/option/add_subject',$data1 + $data2 + $data3);
			//redirect('registrar/add_subject');
			
		}
		else
		{
			
			redirect('registrar/studentlist');
			
		}

	}
	public function download_registrar_sub()
	{
		if ($this->session->userdata('is_logged_in')) 
		{
			$student_id =$this->input->get('student_id');
			$this->load->model('Model_users');
			$this->Model_users->download_sub($student_id);
		}
		else
		{
			redirect('registrar/studentlist');
		}
			
		

				
	
	}



}


?>