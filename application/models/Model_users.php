<?php

class Model_users extends CI_Model{

	public function data_exist($student_id)
	{
		$sql = "SELECT * FROM student WHERE student_id='$student_id'";
		$query = $this->db->query($sql);
		return $query;

	}
	public function subject_exist($student_id,$subject_code)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND subject_code = '$subject_code'";
		$query = $this->db->query($sql);
		return $query;

	}
	public function create_subject($student_id,$course,$year_level,$semester,$instructor,$subject_code,$subject_title,$no_units,$prelim,$midterm,$prefinal,$final,$rating)
	{
		$query = "INSERT INTO student_grades(student_id, course, year_level, semester, instructor, subject_code, subject_title, no_units, prelim, midterm, prefinal, final, rating) VALUES('$student_id','$course','$year_level','$semester','$instructor','$subject_code','$subject_title','$no_units','$prelim','$midterm','$prefinal','$final','$rating')";
		$data = $this->db->query($query);
		return $data;

	}

	public function loggedin($username,$password)
	{
		//$pass=md5($password);
		$sql = "SELECT * FROM registrar WHERE lastname='$username' AND password='$password'";
		$query = $this->db->query($sql);
		return $query;
	}
	public function create_student($student_id,$student_pass,$opt_not,$sname,$fname,$mname,$gender,$age,$date,$status,$citizenship,$haddress,$tnumber,$cnumber,$eaddress,$course,$major_in,$school_year,$year_level,$semester)
	{
		$query = "INSERT INTO student(student_id,student_password,student_status,sur_name,first_name,middle_name,gender,age,date_of_birth,status,citizenship,home_address,tel_no,cell_no,email_address,course,major_in,school_year,year_level,semester) VALUES('$student_id','$student_pass','$opt_not','$sname','$fname','$mname','$gender','$age','$date','$status','$citizenship','$haddress','$tnumber','$cnumber','$eaddress','$course','$major_in','$school_year','$year_level','$semester')";
		$this->db->query($query);
		return true;

	}
	public function get_students()
	{
		$sql = "SELECT * FROM student";
		$dat = $this->db->query($sql);
		return $dat;
	}
	public function view_details($registrarNO)
	{
		$sql = "SELECT * FROM student WHERE registrarNO='$registrarNO'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function update_this($student_id,$student_pass,$opt_not,$sname,$fname,$mname,$gender,$age,$date,$status,$citizenship,$haddress,$tnumber,$cnumber,$eaddress,$course,$major_in,$school_year,$year_level,$semester)
	{
		
		$query = "UPDATE student SET student_password='$student_pass',student_status='$opt_not',sur_name='$sname',first_name='$fname',middle_name='$mname',gender='$gender',age='$age',date_of_birth='$date',status='$status',citizenship='$citizenship',home_address='$haddress',tel_no='$tnumber',cell_no='$cnumber',email_address='$eaddress',course='$course',major_in='$major_in',school_year='$school_year',year_level='$year_level',semester='$semester' WHERE student_id='$student_id'";
		$data = $this->db->query($query);
		return $data;

	}
	public function get_student_id($student_id)
	{
		$sql = "SELECT * FROM student WHERE student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function get_subject($stud_grades_no)
	{
		$sql = "SELECT * FROM student_grades WHERE stud_grades_no='$stud_grades_no'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function drop_subject($stud_grades_no)
	{
		$sql = "UPDATE student_grades SET rating = 'Dropped' WHERE stud_grades_no='$stud_grades_no'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function sub_update($stud_grades_no,$student_id,$course,$year_level,$semester,$instructor,$subject_code,$subject_title,$no_units,$prelim,$midterm,$prefinal,$final,$rating)
	{
		
		$query = "UPDATE student_grades SET student_id = '$student_id',course = '$course',year_level='$year_level',semester='$semester',instructor='$instructor',subject_code='$subject_code',subject_title='$subject_title',no_units='$no_units',prelim='$prelim',midterm='$midterm',prefinal='$prefinal',final='$final',rating = '$rating' WHERE stud_grades_no='$stud_grades_no'";
		$data = $this->db->query($query);
		return $data;

	}
	public function sub_updateModal($stud_grades_no)
	{
		
		$this->db->from('student_grades');
		$this->db->where('stud_grades_no',$stud_grades_no);
		$query = $this->db->get();

		return $query->row();

	}
	
	public function view_det($username)
	{


		$condition = "student_id =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('student');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return $query->result();
		} else {
		return false;
		}
		

	}
	public function loggedin_student($data)
	{
		//$pass=md5($password);
		$condition = "student_id =" . "'" . $data['username'] . "' AND " . "student_password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from('student');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return true;
		} else {
		return false;
		}
		
	}
	public function loggedin_registrar($data)
	{
		$condition = "registrar_id =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from('registrar');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return true;
		} else {
		return false;
		}
		
	}
	public function view_registrar($username)
	{


		$condition = "registrar_id =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('registrar');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return $query->result();
		} else {
		return false;
		}
		

	}
	public function full_student_details($student_id)
	{
		//$this->db->select('*');
		//$this->db->where('student_id',$student_id);
		//$data = $this->db->get('student');
		//return $data->result();

		$data =$this->db->get_where('student',array('student_id'=>$student_id));
	}

	public function download_file()
	{
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');

		$sql = "SELECT student_number, student_id , student_status, sur_name, first_name, middle_name, gender, age, date_of_birth, status, citizenship, home_address, tel_no, cell_no, email_address, course, major_in, school_year, year_level, semester FROM student";
		$query = $this->db->query($sql);

		
		$delimiter = ',';
		$newline = "\r\n";
		$tab = "\t";
		$data = $this->dbutil->csv_from_result($query,$delimiter,$newline,$tab);

		write_file('./public/uploads/client.csv',"sep=,\n".$data);

		force_download('List of Student.csv',$data);

	}
	public function download_sub($student_id)
	{

		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');

		$sql = "SELECT course, year_level, semester, instructor, subject_code, subject_title, no_units, prelim, midterm, prefinal, final, rating FROM student_grades WHERE student_id='$student_id' ORDER BY year_level";
		$query = $this->db->query($sql);

		
		$delimiter = ',';
		$newline = "\r\n";
		$tab = "\t";
		$data = $this->dbutil->csv_from_result($query,$delimiter,$newline,$tab);

		write_file('./public/uploads/client.csv',"sep=,\n".$data);

		force_download('List of Subjects.csv',$data);

	}
	public function change_password($username,$old_pass,$new_pass)
	{
		$sql = "UPDATE student SET student_password = '$new_pass' WHERE student_id = '$username' AND student_password = '$old_pass'";
		$this->db->query($sql);
		return true;

	}
	public function change_password_reg($registrar_id,$old_pass,$new_pass)
	{
		$sql = "UPDATE registrar SET password = '$new_pass' WHERE registrar_id = '$registrar_id' AND password = '$old_pass'";
		$this->db->query($sql);
		return true;

	}

	public function search_to_change_reg($registrar_id,$old_pass)
	{
		
		$sql = "SELECT * FROM registrar where registrar_id='$registrar_id' AND password='$old_pass'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function search_to_change($username,$old_pass)
	{
		
		$sql = "SELECT * FROM student where student_id='$username' AND student_password='$old_pass'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function insert_curid($student_id)
	{
		$query = "INSERT INTO student_grades(student_id) VALUES('$student_id')";
		$this->db->query($query);
		return true;
	}

	public function get_allsubjects($username)
	{
		$sql = "SELECT * FROM student_grades where student_id='$username'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function get_student_data($student_id)
	{
		$sql = "SELECT * FROM student where student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function get_student_sub($student_id)
	{
		$sql = "SELECT * FROM student_grades where student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function get_student_data1($student_id)
	{
		$sql = "SELECT * FROM student where student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function get_student_sub1($student_id)
	{
		$sql = "SELECT * FROM student_grades where student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}

	public function load_sub($course)
	{
		$sql = "SELECT * FROM subjects_demo WHERE course='$course'";
		$data = $this->db->query($sql);
		return $data;
	}

	public function insert_images($image_data=array(),$student_id)
	{
		$data = array(
			'student_id'=>$student_id,
			'filename' => $image_data['file_name'],
			'fullpath' => $image_data['full_path']
			);
		$this->db->insert('profile_picture',$data);
		
	}
	public function insert_profile_id($student_id)
	{
		$query = "INSERT INTO profile_picture(student_id) VALUES('$student_id')";
		$data = $this->db->query($query);
		return $data;
	}
	public function update_images($image_data=array(),$student_id)
	{
		$data = array(
			
			
			'filename' => $image_data['file_name'],
			'fullpath' => $image_data['full_path']
			);
		$this->db->where('student_id', $student_id);
		$this->db->update('profile_picture', $data);
		//$this->db->replace('profile_picture',$data);
	}
	public function get_pic($username)
	{
		$condition = "student_id =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('profile_picture');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
		return $query->result();
		} else {
		return false;
		}
		
		
	}

	public function search_picture($student_id)
	{
		$query ="SELECT * FROM profile_picture WHERE student_id = '$student_id'";
		$data = $this->db->query($query);
		return $data;

	}

	public function login_info($student_id)
	{
		$sql = "SELECT * FROM student where student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function firstyear_first($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='1st' AND semester='1st'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function firstyear_second($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='1st' AND semester='2nd'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function secondyear_first($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='2nd' AND semester='1st'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function secondyear_second($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='2nd' AND semester='2nd'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function thirdyear_first($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='3rd' AND semester='1st'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function thirdyear_second($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='3rd' AND semester='2nd'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function fourthyear_first($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='4th' AND semester='1st'";
		$data = $this->db->query($sql);
		return $data;
	}
	public function fourthyear_second($student_id)
	{
		$sql = "SELECT * FROM student_grades WHERE student_id='$student_id' AND year_level='4th' AND semester='2nd'";
		$data = $this->db->query($sql);
		return $data;
	}

	public function find_id($student_id)
	{
		$sql = "SELECT * FROM student WHERE student_id='$student_id'";
		$data = $this->db->query($sql);
		return $data;
	}

	public function sent_request($student_id,$email_address,$student_concern)
	{
		$sql = "INSERT INTO concern(student_id,email_address,student_concern) VALUES('$student_id','$email_address','$student_concern'";
		$data = $this->db->query($sql);
		return $data;
	}




}





?>