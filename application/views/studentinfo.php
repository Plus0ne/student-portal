<!DOCTYPE html>
<html lang="en">
<?php
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
$student_password = ($this->session->userdata['logged_in']['password']);
$student_status = ($this->session->userdata['logged_in']['student_status']);
$sur_name = ($this->session->userdata['logged_in']['sur_name']);
$first_name = ($this->session->userdata['logged_in']['first_name']);
$middle_name = ($this->session->userdata['logged_in']['middle_name']);
$gender = ($this->session->userdata['logged_in']['gender']);
$age = ($this->session->userdata['logged_in']['age']);
$date_of_birth = ($this->session->userdata['logged_in']['date_of_birth']);
$status = ($this->session->userdata['logged_in']['status']);
$citizenship = ($this->session->userdata['logged_in']['citizenship']);
$home_address = ($this->session->userdata['logged_in']['home_address']);
$tel_no = ($this->session->userdata['logged_in']['tel_no']);
$cell_no = ($this->session->userdata['logged_in']['cell_no']);
$email_address = ($this->session->userdata['logged_in']['email_address']);
$course = ($this->session->userdata['logged_in']['course']);
$major_in = ($this->session->userdata['logged_in']['major_in']);
$school_year = ($this->session->userdata['logged_in']['school_year']);
$year_level = ($this->session->userdata['logged_in']['year_level']);
$semester = ($this->session->userdata['logged_in']['semester']);
} else {
header("location: login");
}
?>
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> My Information | Skill-Power Institute</title>
	<meta name="description" content="Skill-Power Institute">
	<meta name="author" content="renz , romel , madel  , angelyn">
	<meta name="keyword" content="bootstrap , skill, power , portal , institute , spi , Skill-Power Institute">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">
		
		.controls {
			border-bottom: 1px solid #e6e6e6;
			text-align: center;
			margin-left: 30px;
			color: #595959;
			font-size: 18px !important;

		}

		.control-label {
			font-size: 18px;
			color: #008000;
			margin-top: 25px;
			margin-left: 30px;
		}
		.avatar {
			border-radius: 50%;
		}
		
		.image-design
		{
			margin: 10px;
			width: 100px;

			border-radius: 50%;
			border:1px solid gray;
			position: inherit;
			right: 50px;
			cursor: zoom-in;
			
		}
		.image-design-zoom
		{
			margin: 10px;
			width: 100px;

			border-radius: 50%;
			border:1px solid gray;
			position: inherit;
			right: 50px;

			
		}
		#hide
		{
			opacity: 0;
			display: none;
		}
		#hides
		{
			opacity: 0;
			display: none;
		}
		.full_screen
		{
			width: 50%;
			
		}
		

		
	</style>
		
</head>

<body onload="setInterval(onTimerElapsed,3500);">
		<!-- start: Header -->
		<div id="theDiv" class="noti" style="display: inline-block;">
			<div style="position: relative;">
				<img class="noti-img" src="<?=base_url()?>assets/background-for-welcom.png">
			</div>
			
			<div style="padding: 30px;color: white;position: absolute; top: 20px; left: 20px">
			
				<span> Hello! <?php echo $first_name;?> <?php echo $sur_name;?> Good day! Welcome to your Dashboard. </span>
				<span> Any concern? please contact the registrar.</span> 
			</div>
		</div>
	<script type="text/javascript">
		function onTimerElapsed()
		{
			var myDiv = document.getElementById('theDiv');
			myDiv.style.display = myDiv.style.display = 'none';
			
		}
	</script>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?=base_url()?>student/studentinfo"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span>Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Hi! <?php echo $first_name;?> <?php echo $sur_name;?>
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" style="width: 200px;">
								<li class="dropdown-menu-title">
									<center>
									<?php
											$this->load->model('Model_users');
											
											?>
												<?php $user_profile = $this->Model_users->get_pic($username);?>
												<?php foreach ($user_profile as $row) { ?>
											<img class="image-design" data-target="#full_view_prof" data-toggle="modal" src="<?php echo base_url('upload/'.$row->filename)?>">


										<?php
										}
									?>
									</center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pic" data-toggle="modal"><i class="halflings-icon user"></i> Profile Picture</a></li>
								<li><a href="#" class="btn-setting" data-target="#change_password" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>

								<li><a href="logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="<?=base_url()?>student/studentinfo"><i class="icon-credit-card"></i><span class="hidden-tablet"> My Information</span></a></li>	
						
						<li><a href="<?=base_url()?>student/curriculum"><i class="icon-book"></i><span class="hidden-tablet"> Curriculum</span></a></li>
						
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?=base_url()?>student/studentinfo">My Information</a></li>
			</ul>
			<div class="row-fluid">

				<div class="border-pers" style="width: 97%;">

							<!--<h1>Students Personal Background</h1><br>

							<div class="break_line"></div>-->
							
							<table cellpadding="10px" cellspacing="10px" style="margin-left: 50px;">

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Student ID:</strong></label></td>
									<td><label class="label_des"><?php echo $username;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Password:</strong></label></td>
									<td><label class="label_des"><p id="shown" style="opacity: 0;"><?php echo $student_password;?></p></label></td>
									<td>
										<input id="checked" onclick="show_password()" type="checkbox" name="">Show Password
									</td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Student Status:</strong></label></td>
									<td><label class="label_des"><?php echo $student_status;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Last Name:</strong></label></td>
									<td><label class="label_des"><?php echo $sur_name;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>First Name:</strong></label></td>
									<td><label class="label_des"><?php echo $first_name;?></label></td>

								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Middle Initial/Name:</strong></label></td>
									<td><label class="label_des"><?php echo $middle_name;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Gender:</strong></label></td>
									<td><label class="label_des"><?php echo $gender;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Age:</strong></label></td>
									<td><label class="label_des"><?php echo $age;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Date of Birth:</strong></label></td>
									<td><label class="label_des"><?php echo $date_of_birth;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Status:</strong></label></td>
									<td><label class="label_des"><?php echo $status;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Citizenship:</strong></label></td>
									<td><label class="label_des"><?php echo $citizenship;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Home Address:</strong></label></td>
									<td><label class="label_des"><?php echo $home_address;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Telephone NO:</strong></label></td>
									<td><label class="label_des"><?php echo $tel_no;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Cellphone NO:</strong></label></td>
									<td><label class="label_des"><?php echo $cell_no;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Email Address:</strong></label></td>
									<td><label class="label_des"><?php echo $email_address;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Course:</strong></label></td>
									<td><label class="label_des"><?php echo $course;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Major in:</strong></label></td>
									<td><label class="label_des"><?php echo $major_in;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>School Year</strong></label></td>
									<td><label class="label_des"><?php echo $school_year;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Year Level</strong></label></td>
									<td><label class="label_des"><?php echo $year_level;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Semester</strong></label></td>
									<td><label class="label_des"><?php echo $semester;?></label></td>
								</tr>
								
							</table>

							<h1></h1>

							<div class="break_line"></div> 
					</div>
				
			</div>
       

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->

	<!-- Change Profile Picture -->	
	<div class="modal hide fade" id="change_pic" style="width:300px !important;">
		<div class="modal-header">
			<!--<button type="button" class="close" data-dismiss="modal">×</button>-->
			<h3>Change Your Profile Picture</h3>
		</div>
		<?php echo form_open_multipart('student/do_upload');?>
		
		<div class="modal-body">
			<center>
				<?php
						$this->load->model('Model_users');
						#$pic = $this->Model_users->get_pic();
						?>
							<?php $user_profile = $this->Model_users->get_pic($username);?>
							<?php foreach ($user_profile as $row) { ?>
						<img class="image-design" src="<?php echo base_url('upload/'.$row->filename)?>">


					<?php
					}
				?>
			</center>
			<div class="controls">
				<input id="hide" type="text" name="student_id" value="<?php echo $username;?>">
								<div id="uniform-fileInput" class="uploader"><input class="input-file uniform_on" id="fileInput" type="file" name="userfile"><span style="-moz-user-select: none;" class="filename">No file selected</span><span style="-moz-user-select: none;" class="action">Choose File</span></div>
							  </div>

			
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<input type="submit" class="btn btn-primary" value="Save">
		</div>
		</form>
	</div>

	<!-- Change Profile Picture End -->

	<!-- Change Password -->
	<div class="modal hide fade" id="change_password">
		<div class="modal-header">
			
			<h3>Change Password</h3>
		</div>
		<form action="change_password" method="post">
		<div class="modal-body">
			<div class="controls">

				
					<input id="hides" type="text" name="username" placeholder="Student Number" required="" value="<?php echo $username;?>"><br>
					<input type="password" name="old_pass" placeholder="Old Password" required=""><br>
					<input type="text" name="new_pass" placeholder="New Password" required=""><br>
				
								
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" class="btn" class="cp_btn" data-dismiss="modal">Close</button>
			<button class="btn btn-primary" class="btn" class="cp_btn" type="submit">Save changes</button></a>
		</div>
		</form>
	
	</div>
	<!-- Change Password End -->
	<!-- Profile large view -->
	<div class="modal hide fade" id="full_view_prof" style="background-color: transparent !important;width: 39%;">
		
		<div class="modal-body">
			
					<?php

						$this->load->model('Model_users');
						
						?>
						<label style="color: white;"><?php echo $row->filename;?></label>
							<?php $user_profile = $this->Model_users->get_pic($username);?>
							<?php foreach ($user_profile as $row) { ?>
						<img class="full_screen" src="<?php echo base_url('upload/'.$row->filename)?>">


					<?php
					}
					?>
								
		
		</div>
		
	
	</div>
	<!-- Profile large view End -->	
	
				
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			

			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
		<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>

		<script src="<?=base_url()?>assets/js-custom-made.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
