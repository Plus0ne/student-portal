<!DOCTYPE html>
<html>
<head>
	<title>
		Student Subjects
	</title>
	<style type="text/css">
		table
		{
			width: 100%;
			text-align: center;
			border: 1px solid black;
		}
		tr , td
		{
			 text-align: left !important;
			text-align: center;
		}
		td
		{
			text-align: center;
			border: 1px solid black;
			color: #003300;
		}
		.border
		{
			border: 1px solid #d9d9d9;
			margin-top: 10px;
			
		}
		body
		{
			font-size: 11px;
		}
		.thead
		{
			border: 1px solid #d9d9d9;
		}
		.tfoot
		{
			border: 1px solid #d9d9d9;
		}
	</style>
</head>
<body>
<div>
	<div>
		<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300">
		<div>
			<h2>Name: <?=$first_name;?> <?=$middle_name;?> <?=$sur_name;?></h2>
			<h2>Course: <?=$course;?></h2>
		</div>
		<div>
			<h2> Subject List with Grades</h2>
		</div>
		<div style="width: 100%; margin-top: 10px;margin-bottom: 10px;float: left;"></div>

		<h3><?=$year_level;?> Year/First Semester</h3>
			<table>
					<tr>
						<th>Code</th>
						<th>Description</th>
						<th>Units</th>
						<th>Instructor</th>
						
						<th>Rating</th>
					</tr>
				
				<?php foreach($download_this_yearf->result() as $row) { ?>
					<tr>
						<td><?php echo $row->subject_code;?></td>
						<td><?php echo $row->subject_title;?></td>
						<td><?php echo $row->no_units;?></td>
						<td><?php echo $row->instructor;?></td>
						<td><?php echo $row->rating;?></td>
						
					</tr>
				<?php } ?>
			</table>

		<h3><?=$year_level;?> Year/Second Semester</h3>
			<table>
					<tr>
						<th>Code</th>
						<th>Description</th>
						<th>Units</th>
						<th>Instructor</th>
						
						<th>Rating</th>
					</tr>
				
				<?php foreach($download_this_years->result() as $row) { ?>
					<tr>
						<td><?php echo $row->subject_code;?></td>
						<td><?php echo $row->subject_title;?></td>
						<td><?php echo $row->no_units;?></td>
						<td><?php echo $row->instructor;?></td>
						<td><?php echo $row->rating;?></td>
						
					</tr>
				<?php } ?>
			</table>
		

		
	</div>
</div>
<div>
	<footer>
		<p>Any concern? Please contact the registrar.</p>
		<p>This is not a valid copy.</p>
	</footer>
</div>
</body>
</html>