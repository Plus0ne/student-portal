<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Student Log-in | Skill-Power Institute</title>
	<meta name="description" content="Skill-Power Institute">
	<meta name="author" content="renz , romel , madel  , angelyn">
	<meta name="keyword" content="bootstrap , skill, power , portal , institute , spi , Skill-Power Institute">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	
			<style type="text/css">
			body {

				/*background-image: url(<?=base_url()?>assets/login-background.png);*/
				background-size: cover;
				
			}
			input {
				color: black !important;
				
			}
			#background_to
			{
				width: 100%;
				
				opacity: 0.5;
				z-index: -1000;
				position: fixed;
			}
			


			
		</style>
		
		
		
</head>

<body>
<img id="background_to" src="<?=base_url()?>assets/login-background.png">
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
			
				<div class="login-box">
					<div class="icons">
						
						<a href="#" style="cursor:default;"><i ></i></a>
						<div class="dropdown">
  							<button class="dropbtn">Select Login</button>
  							<div class="dropdown-content">
   					        <a href="<?=base_url()?>student/login">Student</a>
						    
						    <a href="<?=base_url()?>registrar/login">Registrar</a>
						    </div>
							</div>
						<!-- Modal for Login Settings -->
						<div class="modal hide fade" id="setting">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h2 class="set-login">Login Settings</h2>
							</div>
							<div class="modal-body"   style="height: 200px;">
							
							</div>
							
							
							<div class="modal-footer">
							
							</div>
						</div>
					</div><br> 
					<h2 style=" font-size:25px; margin-top:1%;">Student Login</h2><br>
					<h2 style=" font-size:15px;">Login to your account</h2>
					<form class="form-horizontal" action="<?=base_url()?>Student/login_validation" method="post">
						
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="username" id="username" type="text" placeholder="Student ID"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="Student Password"/>
							</div>
							
							
								
								
							
							<div class="clearfix"></div>
							
							
							<div class="login-btn" style="cursor: pointer;">
								<button class="btn-login" type="submit"> Login </button>
							</div>
							<div class="clearfix"></div>
							

					</form>
					<hr>
				 <br>
					
					
					<p>
						Any Concern? <a style="cursor:pointer;" data-target="#fp-setting" data-toggle="modal">click here</a> to contact the registrar.
					</p>	



				<div class="modal hide fade" id="fp-setting">
					<div class="modal-header">
					<form action="<?=base_url()?>student/concern" method="post">
					<h2>Contact Registrar</h2>
					</div>
					<div class="modal-body" style="text-align: left;">
						
					
						<label>Student ID</label>
						<input type="text" name="student_id" required="">
						<label>Email Address</label>
						<input type="text" name="email_address" required="">
						<label>Concern</label>
						<textarea name="student_concern" style="text-align: left; width: 300px; height: 150px;"></textarea>

					

					</div>
					<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
					<button class="btn btn-primary" type="submit">Send</button>
					</form>
					</div>
				</div>






				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
