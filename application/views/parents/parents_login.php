<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Log-in as Parent | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="../assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="../assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="../assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="../assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="../assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="../assets/style.css">
	
			<style type="text/css">
			body {
				background-image: url(../assets/login-background.png);
				background-size: cover;
				font-family: century gothic !important;
			}
			input {
				color: black !important;
				font-family: century gothic;
			}
			


			
		</style>
		
		
		
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
			
				<div class="login-box">
					<div class="icons">
						<!--<a href="index.html"><i class="halflings-icon home"></i></a>-->
						<a href="#" style="cursor:default;"><i ></i></a>
						<div class="dropdown">
  							<button class="dropbtn">Select Login</button>
  							<div class="dropdown-content">
   					        <a href="http://localhost/SISwithCI/student/login">Student</a>
						    <a href="http://localhost/SISwithCI/parents/parents_login">Parent</a>
						    <a href="http://localhost/SISwithCI/registrar/registrar_login">Registrar</a>
						    </div>
							</div>
						<!-- Modal for Login Settings -->
						<div class="modal hide fade" id="setting">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h2 class="set-login">Login Settings</h2>
							</div>
							<div class="modal-body"   style="height: 200px;">
							
							</div>
							
							
							<div class="modal-footer">
							<!--<a href="#" class="btn" data-dismiss="modal">Close</a>
							<a href="#" class="btn btn-primary">Save changes</a>-->
							</div>
						</div>
					</div><br>
					<h2 style="font-family: century gothic; font-size:25px; margin-top:1%;">Parent's Login</h2><br>
					<h2 style="font-family: century gothic; font-size:15px;">Login to your account</h2>
					<form class="form-horizontal" action="#" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="username" id="username" type="text" placeholder="Parent's ID"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="Password"/>
							</div>
							<div class="clearfix"></div>
							
							<!--<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>-->

							<!--<div class="button-login">	
								<button type="submit" class="btn btn-primary" style="font-family: century gothic; background-color:#4CAF50;">Login</button>
							</div>-->
							<div class="login-btn" style="cursor: pointer;">
								<button class="btn-login" type="submit"> Login </button>
							</div>
							<div class="clearfix"></div>							
					</form>
					<hr>
					<h2><a href="#" style="font-family: century gothic;">Sign-up</a></h2> <br>
					
					<h3 style="font-family: century gothic;">Forgot Password?</h3>
					<p>
						No problem, <a style="cursor:pointer;" data-target="#fp-setting" data-toggle="modal">click here</a> to get a new password.
					</p>	



				<div class="modal hide fade" id="fp-setting">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h2>Forgot Password</h2>
					</div>
					<div class="modal-body">

					<form>
						<label>Student ID</label>
						<input type="text" name="">
						<label>Guardian's Name</label>
						<input type="text" name="">
						<label>New password</label>
						<input type="password" name="">
						<label>Confirm password</label>
						<input type="password" name="">
					</form>
					</div>
					<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
					<a href="#" class="btn btn-primary">Save changes</a>
					
					</div>
				</div>






				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="../assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	<!-- start: JavaScript-->

		<script src="../assets/js/jquery-1.9.1.min.js"></script>
	<script src="../assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="../assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="../assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="../assets/js/modernizr.js"></script>
	
		<script src="../assets/js/bootstrap.min.js"></script>
	
		<script src="../assets/js/jquery.cookie.js"></script>
	
		<script src="../assets/js/fullcalendar.min.js"></script>
	
		<script src="../assets/js/jquery.dataTables.min.js"></script>

		<script src="../assets/js/excanvas.js"></script>
	<script src="../assets/js/jquery.flot.js"></script>
	<script src="../assets/js/jquery.flot.pie.js"></script>
	<script src="../assets/js/jquery.flot.stack.js"></script>
	<script src="../assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="../assets/js/jquery.chosen.min.js"></script>
	
		<script src="../assets/js/jquery.uniform.min.js"></script>
		
		<script src="../assets/js/jquery.cleditor.min.js"></script>
	
		<script src="../assets/js/jquery.noty.js"></script>
	
		<script src="../assets/js/jquery.elfinder.min.js"></script>
	
		<script src="../assets/js/jquery.raty.min.js"></script>
	
		<script src="../assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="../assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="../assets/js/jquery.gritter.min.js"></script>
	
		<script src="../assets/js/jquery.imagesloaded.js"></script>
	
		<script src="../assets/js/jquery.masonry.min.js"></script>
	
		<script src="../assets/js/jquery.knob.modified.js"></script>
	
		<script src="../assets/js/jquery.sparkline.min.js"></script>
	
		<script src="../assets/js/counter.js"></script>
	
		<script src="../assets/js/retina.js"></script>

		<script src="../assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
