<!DOCTYPE html>
<html>
<head>
	<title>
		Concern
	</title>
	<style type="text/css">
		body
		{
			margin: 0px;
		}
		.content-body
		{
			
			width: 100%;
			height: 500px;
		}
		.content
		{
			
			width: 80%;
			
			margin-left: 10%;
			margin-right: 10%;
			padding: 5px;
		}
		.header-img
		{
			height: 150px;
		}
		.line
		{
			width: 100%;
			margin-top: 10px;
			margin-bottom: 10px;
			border-top: 1px solid #e6e6e6;
		}
		.message
		{
			font-size: 16px;
		}
	</style>
</head>
<body>
<div class="content-body">
	<div class="content">
		<img class="header-img" src="http://www.spi.edu.ph/wp-content/uploads/2015/11/spi_logo_transparent_v3.png">
	</div>
	<div class="line"></div>
	<div class="content">
		<p>Student ID: <?=$student_id;?></p>
		<p>From: <?=$first_name;?> <?=$middle_name;?> <?=$sur_name;?></p>
		<p>Course: <?=$course;?></p>
		<p>Year Level: <?=$year_level;?></p>
	<div class="line"></div>
		<p class="message"><?=$concern;?></p>
	</div>
</div>
<div class="footer">
	
</div>
</body>
</html>