<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Sign-up | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="../assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="../assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="../assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="../assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="../assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	
			<style type="text/css">
			body {
				background-image: url(../assets/login-background.png);
				background-repeat: no-repeat;
				background-size: cover;
				font-family: century gothic;
			}
			
			label {
				color: black !important;
			}
			input {
				color: black !important;
			}
			form ,h1{
				text-align: center;
			}
			.sss {
				font-size: 25px;
			}
			/* -------- ADD STUDENT IN REGISTRAR ------*/

.in-row {
    float: left;
    margin-left: 20px;
}
input[type=text]:focus {
            
            box-shadow: 0px 0px 10px -5px green;
        }
input[type=email]:focus {
    box-shadow: 0px 0px 10px -5px green;
}
input[type=password]:focus {
    box-shadow: 0px 0px 10px -5px green;
}

.positioning- {

    margin-left: 20px;
}
.border-pers {
    box-shadow: 0px 0px 10px -5px green;
    padding: 50px 15px 60px 15px;
    float:left;
    margin: 5px 10% 5px 10%;
    
}
.clear-fix {
    border-bottom: 1px solid gray;
    clear: left;
    margin-bottom: 10px;
}
.btn-save-outer {

}
.btn-save-student {
    font-size: 20px;
    color: white;
    background-color: transparent;
    border:none;
    background-color:#4CAF50;
    float: left;
    margin-right: 200px;
    margin-top: 80px;
    text-align: center;
    width: ;
    padding: 10px;
}
.btn-save-student:hover {
    transition: 0.5s;
    background-color: #5fb962;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}





/* -----------------------------------------*/
			
		</style>
		
		
		
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
			
				
					<!--<div class="icons">
						<a href="index.html"><i class="halflings-icon home"></i></a>
						<a href="#"><i class="halflings-icon cog"></i></a>
					</div>--><br>
					
					
										<div class="border-pers">
										<h1 class="header-title">Fill up this form</h1>
										<form action="http://localhost/SISwithCI/registrar/insert_student" method="post">
											<h1 class="sss">Students Personal Background</h1>
											<div class="clear-fix"></div>
											<div class="in-row">
												<label> Studen Number </label>
												<input type="text" name="student_number">
											</div>
											<div class="in-row">
												<label> Password </label>
												<input type="password" name="student_pass">
											</div><br>
											<div class="positioning-">
												<label> </label>
												<select style="margin-left:20px;" name="opt_not">
													<option>New</option>
													<option>Old</option>
													<option>Transferee</option>
												</select>
											</div><br>
											<div class="in-row">
												<label>Surname</label>
												<input type="text" name="sname">
											</div>
											<div class="in-row">
												<label>First Name</label>
												<input type="text" name="fname">
											</div>
											<div class="in-row">
												<label>Middle Name</label>
												<input type="text" name="mname">
											</div>
											<div class="in-row">
												<label> Gender </label>
												<select name="gender">
													<option>Male</option>
													<option>Female</option>
												</select>
											</div>
											<div class="in-row">
												<label>Age</label>
												<input style="width:100px;" type="text" name="age">
											</div>
											<div class="in-row">
												<label>Date of birth</label>
												<!--<select style="width:100px;" name="month">
													<option>January</option>
													<option>Febuary</option>
													<option>March</option>
													<option>April</option>
													<option>May</option>
													<option>June</option>
													<option>July</option>
													<option>August</option>
													<option>September</option>
													<option>October</option>
													<option>November</option>
													<option>December</option>
												</select>
												<input style="width:33px;" type="text" name="day">-->
												<input type="text" name="date">
											</div>
											<div class="in-row">
												<label>Status</label>
												<select style="width:100px;" name="status">
													<option>Single</option>
													<option>Married</option>
													<option>Divorced</option>
												</select>
											</div>
											<div class="in-row">
												<label>Citizenship</label>
												<input type="text" name="citizenship">
											</div>
											<div class="in-row">
												<label>Home Address</label>
												<input style="width:325px;" type="text" name="haddress">
											</div><br>
											<div class="in-row">
												<label>Tel. Number</label>
												<input type="text" name="tnumber">
											</div>
											<div class="in-row">
												<label>Cellphone Number</label>
												<input type="text" name="cnumber">
											</div>
											<div class="in-row">
												<label>Email Address</label>
												<input type="email" name="eaddress">
											</div>
											<div class="in-row">
												<label>Course</label>
												<select name="course">
													<option>Bachelor of Science in Computer Science</option>
													<option>Bachelor of Science in Business Adminstration</option>
												</select>
											</div><br>
											<div class="in-row">
												<label>Major in</label>
												<input type="text" name="major_in">
											</div>
											<div class="in-row">
												<label>School Year</label>
												<input type="text" name="school_year">
											</div>
											<div class="in-row">
												<label>Year Level</label>
												<select style="width:100px;" name="year_level">
													<option>1st Year</option>
													<option>2nd Year</option>
													<option>3rd Year</option>
													<option>4th Year</option>
												</select>
											</div>
											<div class="in-row">
												<label>Semester</label>
												<select style="width:120px;" name="semester">
													<option>1st Semester</option>
													<option>2nd Semester</option>
												</select>
											</div>
											<div class="in-row">
												
												<button type="submit" class="btn-save-student"> Save Student </button>
											</div>
											</form>
										</div>
									

					
				<!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="../assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	<!-- start: JavaScript-->

		<script src="../assets/js/jquery-1.9.1.min.js"></script>
	<script src="../assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="../assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="../assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="../assets/js/modernizr.js"></script>
	
		<script src="../assets/js/bootstrap.min.js"></script>
	
		<script src="../assets/js/jquery.cookie.js"></script>
	
		<script src="../assets/js/fullcalendar.min.js"></script>
	
		<script src="../assets/js/jquery.dataTables.min.js"></script>

		<script src="../assets/js/excanvas.js"></script>
	<script src="../assets/js/jquery.flot.js"></script>
	<script src="../assets/js/jquery.flot.pie.js"></script>
	<script src="../assets/js/jquery.flot.stack.js"></script>
	<script src="../assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="../assets/js/jquery.chosen.min.js"></script>
	
		<script src="../assets/js/jquery.uniform.min.js"></script>
		
		<script src="../assets/js/jquery.cleditor.min.js"></script>
	
		<script src="../assets/js/jquery.noty.js"></script>
	
		<script src="../assets/js/jquery.elfinder.min.js"></script>
	
		<script src="../assets/js/jquery.raty.min.js"></script>
	
		<script src="../assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="../assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="../assets/js/jquery.gritter.min.js"></script>
	
		<script src="../assets/js/jquery.imagesloaded.js"></script>
	
		<script src="../assets/js/jquery.masonry.min.js"></script>
	
		<script src="../assets/js/jquery.knob.modified.js"></script>
	
		<script src="../assets/js/jquery.sparkline.min.js"></script>
	
		<script src="../assets/js/counter.js"></script>
	
		<script src="../assets/js/retina.js"></script>

		<script src="../assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
