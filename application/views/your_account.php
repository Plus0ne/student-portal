<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		Your Student ID and Password
	</title>
	<style type="text/css">
		body
		{
			font-family: arial;
			font-size: 18px;
			text-decoration: none;

		}
		a
		{
			text-decoration: none;
			color: black;
		}
		a:hover
		{
			color: gray;
			text-decoration: underline;
		}
		.body-container
		{
			float: left;
			
			width: 100%;
			
		}
		.body-content
		{
			float: left;
			margin: 20px;
			width: 96%;
		}
		table
		{
			padding: 10px;
		}
		tr
		{
			padding: 10px;
		}
		td
		{
			padding: 6px;
		}

	</style>
</head>
<body>
<div class="body-container">
	<div class="body-content">
	<div class="img-head">
		<img src="http://www.spi.edu.ph/wp-content/uploads/2015/11/spi_logo_transparent_v3.png">
	</div>

	<div style="width: 100%;float: left; border-top: 1px solid #e6e6e6;"></div>
	<div style="margin: 60px;">
	<?php foreach ($login_info->result() as $row) { ?>
		<p>Hello! <strong> <?php echo $row->first_name;?> <?php echo $row->sur_name;?></strong>.</p>
	<?php } ?>
		<p>Welcome to Skill-Power Institute. To view your own personal data and subjects sign-in in our Student Portal.</p>
		<p>This is your:</p>
			<table style="margin: 30px;">
			<?php foreach ($login_info->result() as $row) { ?>
				
				<tr>
					<td>
						Username:
					</td>
					<td>
						<?php echo $row->student_id;?>
					</td>
				</tr>
				<tr>
					<td>
						Password:
					</td>
					<td>
						<?php echo $row->student_password;?>
					</td>
				</tr>
			<?php } ?>
			</table>
		
		

	</div>
	</div>
</div>

<div style="width: 100%;float: left; border-top: 1px solid #e6e6e6;"></div>

<footer style="color: #004d00;float: left;margin: 20px;font-size: 15px;">
	<a href="http://www.spi.edu.ph" style="color: #004d00;" target="_blank"><p>Skill-Power Institute Antiplo Branch</p></a>
	<p>SPI Building Quezon Avenue, Ext. Antipolo City</p>
</footer>
</body>
</html>