<!DOCTYPE html>
<html lang="en">
<?php
if (isset($this->session->userdata['is_logged_in'])) {
$registrar_id = ($this->session->userdata['is_logged_in']['registrar_id']);
$lastname = ($this->session->userdata['is_logged_in']['lastname']);
$firstname = ($this->session->userdata['is_logged_in']['firstname']);
$middlename = ($this->session->userdata['is_logged_in']['middlename']);
$gender = ($this->session->userdata['is_logged_in']['gender']);
$emailaddress= ($this->session->userdata['is_logged_in']['emailaddress']);
$username = ($this->session->userdata['is_logged_in']['username']);
$password = ($this->session->userdata['is_logged_in']['password']);


} else {
header("location: login");
}
?>
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Student List | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">

	</style>
		
</head>

<body onload="setInterval(onTimerElapsed,3500);">
		<!-- start: Header -->
		<div id="theDiv" class="noti" style="display: inline-block;">
			<div style="position: relative;">
				<img class="noti-img" src="<?=base_url()?>assets/background-for-welcom.png">
			</div>
			<div style="padding: 30px;color: white;position: absolute; top: 20px; left: 20px">
				<span> Hello! <?php echo $username;?> Good day! Welcome to your Dashboard. </span> 
			</div>
		</div>
	<script type="text/javascript">
		function onTimerElapsed()
		{
			var myDiv = document.getElementById('theDiv');
			myDiv.style.display = myDiv.style.display = 'none';
			
		}
	</script>
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?=base_url()?>registrar/studentlist"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span class="design-for">Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						

						</li>
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Admin <?php echo $username;?> 

								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="<?=base_url()?>assets/logo.png" width="100px"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pass" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>
								<li><a href="#" class="btn-setting" data-target="#profile_to" data-toggle="modal"><i class="halflings-icon user"></i> Profile Info</a></li>

								<li><a href="logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="<?=base_url()?>registrar/studentlist"><i class="icon-credit-card"></i><span class="hidden-tablet"> Student List</span></a></li>	

						
						<li><a href="<?=base_url()?>registrar/addstudent"><i class="icon-user"></i><span class="hidden-tablet"> Add Student</span></a></li>	

					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?=base_url()?>registrar/studentlist">Student List</a></li>
			</ul>
			<div class="row-fluid">

							<!-- Start here -->
							
					<div class="box-header" data-original-title>
						<h2><i class="icon-file-alt"></i><span class="break"></span>Student List</h2>
						<div class="box-icon">

							<a href="generate_csv" class=""><i class="halflings-icon download-alt"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
					</div>
					</div>
					<div class="box-content"  style="z-index:0 !important;">
					
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  <th>Student Number</th>
								  <th>Last Name</th>
								  <th>First Name</th>
								  <th>Middle Name</th>
								  <th>Gender</th>
								  <th>Course</th>
								  <th>Year Level</th>
								  <th>Action</th>
								  
							  </tr>
						  </thead>   
						  <tbody>
						  <!-- <?php ?>-->

						  <?php foreach($students->result() as $row){ ?>
							<tr class="shadow" style="color: #004d00;">
								<td class="center"><?php echo $row->student_id; ?></td>
								<td class="center"><?php echo $row->sur_name; ?></td>
								<td class="center"><?php echo $row->first_name; ?></td>
								<td class="center"><?php echo $row->middle_name; ?></td>
								
								<td class="center"><?php echo $row->gender; ?></td>
								<td class="center"><?php echo $row->course; ?></td>
								<td class="center"><?php echo $row->year_level; ?></td>
								
								<td class="center" width="80">
								<a href="<?=base_url()?>registrar/add_subject/?student_id=<?php echo $row->student_id;?>&course=<?php echo $row->course;?>"><button class="btn-design">Add Subject</button></a>
								
								<a href="<?=base_url()?>registrar/update_students/?student_id=<?php echo $row->student_id; ?>"><button class="btn-design">Edit info</button></a>
								
								<a href="<?=base_url()?>registrar/full_details/?student_id=<?php echo $row->student_id; ?>"><button class="btn-design">Full Details</button></a>
								
								</td>
							</tr>
							<?php }?>
						
						  </tbody>
					  </table>            
					</div>
				</div>

				
			
			</div>
       

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	<!-- Change Password -->
	<div class="modal hide fade" id="change_pass">
	<form action="<?=base_url()?>registrar/change_password" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<table>
					<tr>
						<td>
							Do you want to change your Password?
						</td>
					</tr>
				</table>
				<table>

					<tr hidden="">
						<td>
							<input type="text" name="registrar_id" value="<?php echo $registrar_id;?>">
						</td>
					</tr>
					<tr>
						<td>
							<input type="password" name="old_pass" placeholder="Enter Old Password">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="new_pass" placeholder="Enter New Password">
						</td>
					</tr>
				</table>
								
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">No</a>
			<button href="#" class="btn btn-primary" type="submit">Yes</button>
		</div>
	</form>
	</div>
	<!-- Change Password -->
	<div class="modal hide fade" id="profile_to">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Profile</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<style type="text/css">
					.info_to
					{
						font-family: century gothic;
						text-align: center;
					}
				</style>
				<table class="info_to">
					<tr><pre>Registrar ID: <?php echo $registrar_id;?></pre></tr>
					<tr><pre>First Name:   <?php echo $firstname;?></pre></tr>
					<tr><pre>Middle Name:  <?php echo $middlename;?></pre></tr>
					<tr><pre>Last Name:    <?php echo $lastname;?></pre></tr>
					<tr><pre>Gender:       <?php echo $gender;?></pre></tr>
					<tr><pre>Username:     <?php echo $username;?></pre></tr>
				</table>			
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<!--<a href="#" class="btn btn-primary">Save changes</a>-->
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left;">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
