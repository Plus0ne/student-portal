<!DOCTYPE html>
<html lang="en">
<?php
if (isset($this->session->userdata['is_logged_in'])) {
$registrar_id = ($this->session->userdata['is_logged_in']['registrar_id']);

$lastname = ($this->session->userdata['is_logged_in']['lastname']);
$firstname = ($this->session->userdata['is_logged_in']['firstname']);
$middlename = ($this->session->userdata['is_logged_in']['middlename']);
$gender = ($this->session->userdata['is_logged_in']['gender']);
$emailaddress= ($this->session->userdata['is_logged_in']['emailaddress']);
$username = ($this->session->userdata['is_logged_in']['username']);
$password = ($this->session->userdata['is_logged_in']['password']);


} else {
header("location: login");
}
?>
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Add New Student | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">

		body
		{

		}
		
		.avatar {
			border-radius: 50%;
		}
		
	
		
		.in-rows
		{
		    float: left;
		    
		    font-size: 18px !important;
		    margin-top: 20px;
		    margin-left: 10px;
		    border-top: none;
		    border-left: none;
		    border-right: none;
		    border-bottom: 1px solid gray;
		    color: black;
		    text-indent: 5px;

		}
		input , select
		{
			float: left;
		}
		
		input[type=text]
		{
			float: left;
		    
		    font-size: 18px !important;
		    margin-top: 20px;
		    margin-left: 20px;
		    border-top: none;
		    border-left: none;
		    border-right: none;
		    border-bottom: 1px solid gray;
		}
		input[type=password]
		{
			float: left;
		    
		    font-size: 18px !important;
		    margin-top: 20px;
		    margin-left: 20px;
		    border-top: none;
		    border-left: none;
		    border-right: none;
		    border-bottom: 1px solid gray;
		}
		input[type=email]
		{
			float: left;
		    
		    font-size: 18px !important;
		    margin-top: 20px;
		    margin-left: 20px;
		    border-top: none;
		    border-left: none;
		    border-right: none;
		    border-bottom: 1px solid gray;
		    width: 328px;
		}
		select {
			
			color: #4d4d4d !important;
			font-size: 18px !important;
		    
		}
		.clear-fixs {
		    border-bottom: 1px solid white;
		    clear: left;
		    margin-bottom: 10px;
		}
		
		
	</style>
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?=base_url()?>registrar/studentlist"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span class="design-for">Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Admin <?php echo $username;?>  
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="<?=base_url()?>assets/logo.png" width="100px"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pass" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>
								<li><a href="#" class="btn-setting" data-target="#profile_to" data-toggle="modal"><i class="halflings-icon user"></i> Profile Info</a></li>
 								<li><a href="logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="<?=base_url()?>registrar/studentlist"><i class="icon-credit-card"></i><span class="hidden-tablet"> Student List</span></a></li>	

						<li><a href="<?=base_url()?>registrar/addstudent"><i class="icon-user"></i><span class="hidden-tablet"> Add Student</span></a></li>	
						
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
		<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?=base_url()?>registrar/addstudent">Add Student</a></li>
			</ul>
			<div class="row-fluid">
							<p id="error"> </p>

							<!-- Start here -->
							
					<form action="<?=base_url()?>registrar/insert_student" method="post">
										<div class="border-pers">
											


												<input style="color: black;" class="in-rows" placeholder="Student ID" type="text" name="student_id" required="">

												<input style="color: black;" class="in-rows" placeholder="Student Password" type="password" name="student_pass" required="">

												<select style="color: black;" class="in-rows" name="opt_not" required="">
													<option disabled selected hidden>Student Status</option>
													<option>Regular</option>
													<option>Iregular</option>
													<option>Dropped</option>
													<option>New</option>
													<option>Old</option>
													<option>Transferee</option>
													<option>Transfered</option>
												</select>

											<div class="clear-fixs"></div>

												<input class="in-rows" placeholder="Last Name" type="text" name="sname" required="">

												<input class="in-rows" placeholder="First Name" type="text" name="fname" required="">

												<input class="in-rows" placeholder="Middle name" type="text" name="mname" required="">
											
											<div class="clear-fixs"></div>

												<select class="in-rows" name="gender" required="">
													<option disabled selected hidden> Gender</option>
													<option>Male</option>
													<option>Female</option>
												</select>

												<input class="in-rows" placeholder="Citizenship" type="text" name="citizenship" required="">

												<select class="in-rows" name="status" required="">
													<option disabled selected hidden>Status</option>
													<option>Single</option>
													<option>Married</option>
													<option>Divorced</option>
												</select>

											<div class="clear-fixs"></div>

												<select class="in-rows" id="month" name="month" required="">
													<option disabled selected hidden>Month</option>
													<option>January</option>
													<option>Febuary</option>
													<option>March</option>
													<option>April</option>
													<option>May</option>
													<option>June</option>
													<option>July</option>
													<option>August</option>
													<option>September</option>
													<option>October</option>
													<option>November</option>
													<option>December</option>
												</select>

												<input class="in-rows" onchange="check_day()" id="day" placeholder="Day" type="text" name="day" required="">

												<input class="in-rows" onchange="chech_year()" id="year" placeholder="Year" type="text" name="year" required="">

												<input class="in-rows" placeholder="Age" id="age" type="text" name="age" required="" readonly="">

											
												
											
											<div class="clear-fixs"></div>

												<input class="in-rows" placeholder="Home Address" style="width:325px;" type="text" name="haddress" required="">
												
												<input class="in-rows" placeholder="Telephone Number" type="text" name="tnumber" required="">
											
											
												<input class="in-rows" placeholder="Cellphone Number" type="text" name="cnumber" required="">
											
											
												
												
											
											
												
												<input class="in-rows" placeholder="Email Address" type="email" name="eaddress" required="">

												<select style="width: 600px;" class="in-rows" name="course" required="">
													<option disabled selected hidden>Course</option>
													<option>Bachelor of Science in Computer Science</option>
													<option>Bachelor of Science in Business Adminstration</option>
												</select>
											
											<div class="clear-fixs"></div>
												
												<input class="in-rows" placeholder="Major in" type="text" name="major_in" required="">
											
										
											
												<input class="in-rows" placeholder="School Year" type="text" name="school_year" required="">

											

												<select class="in-rows" name="year_level" required="">
													<option disabled selected hidden>Year Level</option>
													<option>1st Year</option>
													<option>2nd Year</option>
													<option>3rd Year</option>
													<option>4th Year</option>
												</select>

												<select class="in-rows" name="semester" required="">
													<option disabled selected hidden> Semester</option>
													<option>1st Semester</option>
													<option>2nd Semester</option>
												</select>
											
											
										</div>
										<div>	
											<button type="submit" class="btn-save-student"> Save Student </button>
										</div>
					</form>

			</div>
		</div>
		</div>
		</div>
								
							 
	<!-- Change Password -->
	<div class="modal hide fade" id="change_pass">
	<form action="<?=base_url()?>registrar/change_password" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<table>
					<tr>
						<td>
							Do you want to change your Password?
						</td>
					</tr>
				</table>
				<table>

					<tr hidden="">
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="registrar_id" value="<?php echo $registrar_id;?>">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="password" name="old_pass" placeholder="Enter Old Password">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="new_pass" placeholder="Enter New Password">
						</td>
					</tr>
				</table>
								
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">No</a>
			<button href="#" class="btn btn-primary" type="submit">Yes</button>
		</div>
	</form>
	</div>
	<!-- Change Password -->
	<div class="modal hide fade" id="profile_to">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Profile</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<style type="text/css">
					.info_to
					{
						font-family: century gothic;
						text-align: center;
					}
				</style>
				<table class="info_to">
					<tr><pre>Registrar ID: <?php echo $registrar_id;?></pre></tr>
					<tr><pre>First Name:   <?php echo $firstname;?></pre></tr>
					<tr><pre>Middle Name:  <?php echo $middlename;?></pre></tr>
					<tr><pre>Last Name:    <?php echo $lastname;?></pre></tr>
					<tr><pre>Gender:       <?php echo $gender;?></pre></tr>
					<tr><pre>Username:     <?php echo $username;?></pre></tr>
				</table>			
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<!--<a href="#" class="btn btn-primary">Save changes</a>-->
		</div>
	</div>

	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>
		<script src="<?=base_url()?>assets/js-custom-made.js"></script>

	<!-- end: JavaScript-->
	<!-- custom JAVASCRIPT -->
	
	
</body>
</html>
