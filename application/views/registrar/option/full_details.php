<!DOCTYPE html>
<html lang="en">

<?php
if (isset($this->session->userdata['is_logged_in'])) {
$registrar_id = ($this->session->userdata['is_logged_in']['registrar_id']);

$lastname = ($this->session->userdata['is_logged_in']['lastname']);
$firstname = ($this->session->userdata['is_logged_in']['firstname']);
$middlename = ($this->session->userdata['is_logged_in']['middlename']);
$gender = ($this->session->userdata['is_logged_in']['gender']);
$emailaddress= ($this->session->userdata['is_logged_in']['emailaddress']);
$username = ($this->session->userdata['is_logged_in']['username']);
$password = ($this->session->userdata['is_logged_in']['password']);


} else {
header("location: login");
}
?>





<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Full Details | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">
		
		.controls {
			border-bottom: 1px solid #e6e6e6;
			text-align: center;
			margin-left: 40px;
			color: #595959;

		}

		.control-label {
			font-size: 18px;
			color: #008000;
			margin-top: 25px;
			margin-left: 40px;
		}
		.avatar {
			border-radius: 50%;
		}
		

		
	</style>
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="http://localhost/Student-Portal/registrar/studentlist"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span class="design-for">Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Admin <?php echo $username;?> 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="<?=base_url()?>assets/logo.png" width="100px"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pass" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>
								<li><a href="#" class="btn-setting" data-target="#profile_to" data-toggle="modal"><i class="halflings-icon user"></i> Profile Info</a></li>

								<li><a href="http://localhost/Student-Portal/registrar/logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="http://localhost/Student-Portal/registrar/studentlist"><i class="icon-credit-card"></i><span class="hidden-tablet"> Student List</span></a></li>	

						<li><a href="http://localhost/Student-Portal/registrar/addstudent"><i class="icon-user"></i><span class="hidden-tablet"> Add Student</span></a></li>
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">

			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="http://localhost/Student-Portal/registrar/studentlist">Student List</a></li>
				<li><i class="icon-angle-right"></i></li>
				<li><a href="#"><?php foreach($student_data->result() as $row) {  echo $row->student_id; }?> Full Details</a></li>
			</ul>
			<div class="row-fluid">

							<!-- Start here -->
							



<?php foreach($student_data->result() as $row) {?>	

					<div class="border-pers" style="width: 97%;">

							
<script src="<?=base_url()?>assets/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("fast");
    });
    
});
</script>
 

<div>
	<div class="box-icon">
		
		<div class="box-header" data-original-title>
						<h2><i class="icon-file-alt"></i><span class="break"></span>Student Information</h2>
						<div class="box-icon">
							<strong><img  id="flip" src="<?=base_url()?>assets/toggle.png" width="10px"></strong>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
	</div>
	<div style="width: 100%;margin-top: 20px;margin-bottom: 20px;float: left;"></div>

						<div id="panel" style="display: none;">

							<div class="break_line"></div>
							
							<table cellpadding="10px" cellspacing="10px" style="margin-left: 50px;">

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Student ID:</strong></label></td>
									<td><label class="label_des"><?php echo $row->student_id;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Password:</strong></label></td>
									<td><label class="label_des"><?php echo $row->student_password;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Student Status:</strong></label></td>
									<td><label class="label_des"><?php echo $row->student_status;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Last Name:</strong></label></td>
									<td><label class="label_des"><?php echo $row->sur_name;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>First Name:</strong></label></td>
									<td><label class="label_des"><?php echo $row->first_name;?></label></td>

								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Middle Initial/Name:</strong></label></td>
									<td><label class="label_des"><?php echo $row->middle_name;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Gender:</strong></label></td>
									<td><label class="label_des"><?php echo $row->gender;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Age:</strong></label></td>
									<td><label class="label_des"><?php echo $row->age;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Date of Birth:</strong></label></td>
									<td><label class="label_des"><?php echo $row->date_of_birth;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Status:</strong></label></td>
									<td><label class="label_des"><?php echo $row->status;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Citizenship:</strong></label></td>
									<td><label class="label_des"><?php echo $row->citizenship;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Home Address:</strong></label></td>
									<td><label class="label_des"><?php echo $row->home_address;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Telephone NO:</strong></label></td>
									<td><label class="label_des"><?php echo $row->tel_no;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Cellphone NO:</strong></label></td>
									<td><label class="label_des"><?php echo $row->cell_no;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Email Address:</strong></label></td>
									<td><label class="label_des"><?php echo $row->email_address;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Course:</strong></label></td>
									<td><label class="label_des"><?php echo $row->course;?></label></td>
								</tr>
								
								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Major in:</strong></label></td>
									<td><label class="label_des"><?php echo $row->major_in;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>School Year</strong></label></td>
									<td><label class="label_des"><?php echo $row->school_year;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Year Level</strong></label></td>
									<td><label class="label_des"><?php echo $row->year_level;?></label></td>
								</tr>

								<tr><td></td></tr>

								<tr class="row_design">
									<td><label class="label_des_title"><strong>Semester</strong></label></td>
									<td><label class="label_des"><?php echo $row->semester;?></label></td>
								</tr>
								
							</table>

							<h1></h1>

							<div class="break_line"></div>
						</div>
							 
				 			
							 
					</div>
<?php } ?>

							
<?php $student_id = $this->input->get('student_id');?>
<?php $all_subject = $this->db->query("SELECT * FROM student_grades WHERE student_id='$student_id'");?>

<script src="<?=base_url()?>assets/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function(){
    $("#flip-to").click(function(){
        $("#panel-to").slideToggle("fast");
    });
    
});
</script>
 
<div style="width: 100%;margin-top: 20px;margin-bottom: 20px;float: left;"></div>
<div>
					<div class="box-header" data-original-title>
						<h2><i class="icon-file-alt"></i><span class="break"></span>Subject List</h2>
						<div class="box-icon">
							<a href="<?=base_url()?>registrar/download_registrar_sub/?student_id=<?php echo $row->student_id;?>" class="" title="Download Subjects"><i class="halflings-icon download-alt"></i></a>
							<strong><img  id="flip-to" src="<?=base_url()?>assets/toggle.png" width="10px"></strong>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
					<div id="panel-to" style="display: none;" class="box-content"  style="z-index:0 !important;">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  
							  	  <th class="tbl_header" title="Sort Year Level">Year Level</th>
								  <th class="tbl_header" title="Sort Semester">Semester</th>
								  <th class="tbl_header" title="Sort Instructor">Instructor</th>
								  <th class="tbl_header" title="Sort Code">Code</th>
								  <th class="tbl_header" title="Sort Description">Description</th>
								  <th class="tbl_header" title="Sort No. Units">No. Units</th>
								  <th class="tbl_header" title="Sort Prelim">Prelim</th>
								  <th class="tbl_header" title="SortMidterm">Midterm</th>
								  <th class="tbl_header" title="Sort Pre-final">Pre-final</th>
								  <th class="tbl_header" title="Sort Final">Final</th>
								  <th class="tbl_header" title="Sort Rating">Rating</th>
							  </tr>
						  </thead>   
						  <tbody>
						  <!-- <?php ?>-->

						 <?php foreach($all_subject->result() as $row) {?>
							<tr>
								
								<td class="center"><?php echo $row->year_level;?></td>
								<td class="center"><?php echo $row->semester;?></td>
								<td class="center"><?php echo $row->instructor;?></td>
								<td class="center"><?php echo $row->subject_code;?></td>
								<td class="center"><?php echo $row->subject_title;?></td>
								<td class="center"><?php echo $row->no_units;?></td>
								<td class="center"><?php echo $row->prelim;?></td>
								<td class="center"><?php echo $row->midterm;?></td>
								<td class="center"><?php echo $row->prefinal;?></td>
								<td class="center"><?php echo $row->final;?></td>
								<td class="center"><?php echo $row->rating;?></td>
							</tr>
						<?php } ?>
						
						  </tbody>
					  </table>            
					


<!--____________________________________________end____________________________________________________-->


				
				

			</div>

				
					
					
			</div>
			
		</div>
       

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<!-- Change Password -->
	<div class="modal hide fade" id="change_pass">
	<form action="<?=base_url()?>registrar/change_password" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<table>
					<tr>
						<td>
							Do you want to change your Password?
						</td>
					</tr>
				</table>
				<table>

					<tr hidden="">
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="registrar_id" value="<?php echo $registrar_id;?>">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="password" name="old_pass" placeholder="Enter Old Password">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="new_pass" placeholder="Enter New Password">
						</td>
					</tr>
				</table>
								
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">No</a>
			<button href="#" class="btn btn-primary" type="submit">Yes</button>
		</div>
	</form>
	</div>
	<!-- Change Password -->
	<div class="modal hide fade" id="profile_to">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Profile</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<style type="text/css">
					.info_to
					{
						font-family: century gothic;
						text-align: center;
					}
				</style>
				<table class="info_to">
					<tr><pre>Registrar ID: <?php echo $registrar_id;?></pre></tr>
					<tr><pre>First Name:   <?php echo $firstname;?></pre></tr>
					<tr><pre>Middle Name:  <?php echo $middlename;?></pre></tr>
					<tr><pre>Last Name:    <?php echo $lastname;?></pre></tr>
					<tr><pre>Gender:       <?php echo $gender;?></pre></tr>
					<tr><pre>Username:     <?php echo $username;?></pre></tr>
				</table>			
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<!--<a href="#" class="btn btn-primary">Save changes</a>-->
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
		<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
