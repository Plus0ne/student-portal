<!DOCTYPE html>
<html lang="en">
<?php
if (isset($this->session->userdata['is_logged_in'])) {
$registrar_id = ($this->session->userdata['is_logged_in']['registrar_id']);

$lastname = ($this->session->userdata['is_logged_in']['lastname']);
$firstname = ($this->session->userdata['is_logged_in']['firstname']);
$middlename = ($this->session->userdata['is_logged_in']['middlename']);
$gender = ($this->session->userdata['is_logged_in']['gender']);
$emailaddress= ($this->session->userdata['is_logged_in']['emailaddress']);
$username = ($this->session->userdata['is_logged_in']['username']);
$password = ($this->session->userdata['is_logged_in']['password']);


} else {
header("location: login");
}
?>

<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Update | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">
		
		.controls {
			border-bottom: 1px solid #e6e6e6;
			text-align: center;
			margin-left: 60px;
			color: #595959;

		}

		.control-label {
			font-size: 18px;
			color: #008000;
			margin-top: 25px;
			margin-left: 60px;
		}
		.avatar {
			border-radius: 50%;
		}
		
		
		
		input.edit_btn
		{
			border: none;
			text-align: left;
			margin: 2px;
			width: 95%;
		}
		td
		{
			padding: 0px;
		}
		input.unique_css
		{
			border:none;
			width: 50px;
		}
		
		.clear-fixs {
		    border-bottom: 1px solid white;
		    clear: left;
		    margin-bottom: 10px;
		}

		
	</style>
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="<?=base_url()?>registrar/studentlist"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span>Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- end: Message Dropdown -->
						<!--<li class="">
							<a class="btn" href="#">
								<i class="halflings-icon white wrench"></i>
							</a>

						</li>-->
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Admin <?php echo $username;?> 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="<?=base_url()?>assets/logo.png" width="100px"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pass" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>
								<li><a href="#" class="btn-setting" data-target="#profile_to" data-toggle="modal"><i class="halflings-icon user"></i> Profile Info</a></li>

								<li><a href="<?=base_url()?>registrar/logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
	

						<li><a href="<?=base_url()?>registrar/studentlist"><i class="icon-credit-card"></i><span class="hidden-tablet"> Student List</span></a></li>	
						<!--<li><a href="<?=base_url()?>thesis/messages"><i class="icon-comments"></i><span class="hidden-tablet"> Messages</span></a></li>-->

						<li><a href="<?=base_url()?>registrar/addstudent"><i class="icon-user"></i><span class="hidden-tablet"> Add Student</span></a></li>
						
						
						
						
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?=base_url()?>registrar/studentlist">Student List</a></li>
				<li><i class="icon-angle-right"></i></li>

				<li><a href="#">Edit <?php foreach($student_data->result() as $row) {  echo $row->student_id; }?></a></li>
			</ul>
			<div class="row-fluid">

							<!-- Start here -->
							

<!--____________________________________________Information____________________________________________________-->



<?php foreach($student_data->result() as $row) {?>
					
					
					<form action="<?=base_url()?>registrar/update_student_info" method="post">
										<div class="border-pers">
											<h1>Edit Personal Background</h1>
											<div class="clear-fix"></div>
											<div class="in-row">
												<label> Studen Number </label>
												<input readonly="" type="text" name="student_id" value="<?php echo $row->student_id;?>">
											</div>
											<div class="in-row">
												<label> Password </label>
												<input type="text" name="student_pass" value="<?php echo $row->student_password;?>" required="">
											</div><br>
											<div class="positioning-">
												<label> </label>
												<select style="margin-left:20px;" name="opt_not" required="">
													<option selected="" hidden=""><?php echo $row->student_status;?></option>
													<option>New</option>
													<option>Old</option>
													<option>Transferee</option>
												</select>
											</div>
											<div class="line_break"></div>
											<div class="in-row">
												<label>Surname</label>
												<input type="text" name="sname" value="<?php echo $row->sur_name;?>" required="">
											</div>
											<div class="in-row">
												<label>First Name</label>
												<input type="text" name="fname" value="<?php echo $row->first_name;?>" required="">
											</div>
											<div class="in-row">
												<label>Middle Name</label>
												<input type="text" name="mname" value="<?php echo $row->middle_name;?>" required="">
											</div>
											<div class="in-row">
												<label> Gender </label>
												<select name="gender" required="">
													<option selected="" hidden=""><?php echo $row->gender;?></option>
													<option>Male</option>
													<option>Female</option>
												</select>
											</div>
											<div class="line_break"></div>
											<div class="in-row">
												<label>Age</label>
												<input style="width:100px;" type="text" name="age" value="<?php echo $row->age;?>" required="">
											</div>
											<div class="in-row">
												<label>Date of birth</label>
												<!--<select style="width:100px;" name="month">
													<option>January</option>
													<option>Febuary</option>
													<option>March</option>
													<option>April</option>
													<option>May</option>
													<option>June</option>
													<option>July</option>
													<option>August</option>
													<option>September</option>
													<option>October</option>
													<option>November</option>
													<option>December</option>
												</select>
												<input style="width:33px;" type="text" name="day">-->
												<input type="text" name="date" value="<?php echo $row->date_of_birth;?>" required="">
											</div>
											<div class="in-row">
												<label>Status</label>
												<select style="width:100px;" name="status" required="">
													<option selected="" hidden=""><?php echo $row->status;?></option>
													<option>Single</option>
													<option>Married</option>
													<option>Divorced</option>
												</select>
											</div>
											<div class="in-row">
												<label>Citizenship</label>
												<input type="text" name="citizenship" value="<?php echo $row->citizenship;?>" required="">
											</div>
											<div class="line_break"></div>
											<div class="in-row">
												<label>Home Address</label>
												<input style="width:325px;" type="text" name="haddress" value="<?php echo $row->home_address;?>" required="">
											</div><br>
											<div class="in-row">
												<label>Tel. Number</label>
												<input type="text" name="tnumber" value="<?php echo $row->tel_no;?>" required="">
											</div>
											<div class="in-row">
												<label>Cellphone Number</label>
												<input type="text" name="cnumber" value="<?php echo $row->cell_no;?>" required="">
											</div>
											<div class="line_break"></div>
											<div class="in-row">
												<label>Email Address</label>
												<input type="email" name="eaddress" value="<?php echo $row->email_address;?>" required="">
											</div>
											<div class="in-row">
												<label>Course</label>
												<select name="course" required="">
													<option selected="" hidden=""><?php echo $row->course;?></option>
													<option>Bachelor of Science in Computer Science</option>
													<option>Bachelor of Science in Business Adminstration</option>
												</select>
											</div><br>
											<div class="in-row">
												<label>Major in</label>
												<input type="text" name="major_in" value="<?php echo $row->major_in;?>" required="">
											</div>
											<div class="in-row">
												<label>School Year</label>
												<input type="text" name="school_year" value="<?php echo $row->school_year;?>" required="">
											</div>
											<div class="line_break"></div>
											<div class="in-row">
												<label>Year Level</label>
												<select style="width:100px;" name="year_level" required="">
													<option selected="" hidden=""><?php echo $row->year_level;?></option>
													<option>1st Year</option>
													<option>2nd Year</option>
													<option>3rd Year</option>
													<option>4th Year</option>
												</select>
											</div>
											<div class="in-row">
												<label>Semester</label>
												<select style="width:120px;" name="semester" required="">
													<option selected="" hidden=""><?php echo $row->semester;?></option>
													<option>1st Semester</option>
													<option>2nd Semester</option>
												</select>
											</div>
											<div class="clear-fix"></div>
										<div>
												
												<button type="submit" class="btn-save-student"> Update Student </button>
										</div>
											
										</div>
										
									</form>
<?php } ?>
			
			<div style="width: 100%;margin-top: 10px;margin-bottom: 10px;float: left;"></div>
				
			<div style="width: 100%;margin-top: 10px;margin-bottom: 10px;float: left;"></div>
			
			<div class="border-pers" style="width: 98%;">
					<h1>Edit Subjects</h1>
							<!-- Start here -->
					<div class="box-header" data-original-title>
						<h2><i class="icon-file-alt"></i><span class="break"></span>Subject List</h2>
						<div class="box-icon">
							<!--<a href="#flip" class="" title="Add Subject"><i class="halflings-icon plus-sign"></i></a>
							<a href="<?=base_url()?>registrar/download_registrar_sub/?student_id=<?php echo $row->student_id;?>" class="" title="Download Subjects"><i class="halflings-icon download-alt"></i></a>-->
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<!--<a href="#" class="btn-close"><i class="halflings-icon-remove"></i></a>-->
						</div>
					</div>
					<div class="box-content"  style="z-index:0 !important;">

						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  <th class="tbl_header" title="Sort Year Level">Year Level</th>
								  <th class="tbl_header" title="Sort Semester">Semester</th>
								  <th class="tbl_header" title="Sort Instructor">Instructor</th>
								  <th class="tbl_header" title="Sort Code">Code</th>
								  <th class="tbl_header" title="Sort Description">Description</th>
								  <th class="tbl_header" title="Sort No. Units">No. Units</th>
								  <th class="tbl_header" title="Sort Prelim">Prelim</th>
								  <th class="tbl_header" title="SortMidterm">Midterm</th>
								  <th class="tbl_header" title="Sort Pre-final">Pre-final</th>
								  <th class="tbl_header" title="Sort Final">Final</th>
								  <th class="tbl_header" title="Sort Rating">Rating</th>
								  <th class="tbl_header" title="Sort Rating">Action</th>
								  
							  </tr>
						  </thead> 

						  <tbody>
						  <!-- <?php ?>-->

						 
						  <?php foreach($student_sub->result() as $row) {?>
							<tr>
								<td class="center"><?php echo $row->year_level;?></td>
								<td class="center"><?php echo $row->semester;?></td>
								<td class="center"><?php echo $row->instructor;?></td>
								<td class="center"><?php echo $row->subject_code;?></td>
								<td class="center"><?php echo $row->subject_title;?></td>
								<td class="center"><?php echo $row->no_units;?></td>
								<td class="center"><?php echo $row->prelim;?></td>
								<td class="center"><?php echo $row->midterm;?></td>
								<td class="center"><?php echo $row->prefinal;?></td>
								<td class="center"><?php echo $row->final;?></td>
								<td class="center"><?php echo $row->rating;?></td>
								<td class="center" width="100px;">
								
								<a href="<?php echo base_url('registrar/update_subject/?stud_grades_no='.$row->stud_grades_no)?>"><button class="btn-design">Edit</button></a>
								<!--
								<a href="<?=base_url()?>registrar/drop_subject/?stud_grades_no=<?php echo $row->stud_grades_no;?>&student_id=<?php echo $row->student_id;?>"><button class="drop-design">Drop</button></a>
								
								<button class="btn-design" data-toggle="modal" data-target="#editModal-<?php echo $row->stud_grades_no;?>">Edit</button>-->
								<button class="drop-design" data-toggle="modal" data-target="#dropModal-<?php echo $row->stud_grades_no;?>">Drop</button>
									
								</td>
								
							</tr>
								
							
						<?php } ?>
						
						  </tbody>
						  
					  </table>            
					</div>
					</div>
					<!-- 
					Drop Subject
					-->
						<?php foreach($student_sub->result() as $row) {?>
						<div class="modal hide fade" id="dropModal-<?php echo $row->stud_grades_no;?>">
									<div class="modal-header">
										
										<h3>Warning!</h3>
									</div>
									<div class="modal-body">
										Do you want to drop this subject?
									</div>
									<div class="modal-footer">
										<!--<a href="#" class="btn" data-dismiss="modal">Close</a>-->
										<button style="float: right;width: 150px; font-size: 16px;padding: 10px;" type="button" class="btn-design" data-dismiss="modal">Cancel</button>
										<a href="<?=base_url()?>registrar/drop_subject/?stud_grades_no=<?php echo $row->stud_grades_no;?>&student_id=<?php echo $row->student_id;?>"><button style="float: right;width: 150px; font-size: 16px;padding: 10px;" class="drop-design">Drop This Subject</button></a>

									</div>
								</div>
						<?php } ?>
					<!-- 
					Drop Subject
					-->
					<!-- 
					Edit Subject
					-->
						<?php foreach($student_sub->result() as $row) {?>
						<div class="modal hide fade" id="editModal-<?php echo $row->stud_grades_no;?>">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h3> Edit this subject</h3>
									</div>
									<form action="" method="post">
									<div class="modal-body" style="text-align: left;">

										<input id="stud_grades_no-<?php echo $row->stud_grades_no;?>" type="text" name="stud_grades_no" value="<?php echo $row->stud_grades_no;?>">

										<input id="student_id-<?php echo $row->stud_grades_no;?>" type="text" name="student_id" value="<?php echo $row->student_id;?>">

										<input id="course-<?php echo $row->stud_grades_no;?>" type="text" name="course" value="<?php echo $row->course;?>">

										<div class="">
										<label>Year Level</label>
										<input id="year_level-<?php echo $row->stud_grades_no;?>" type="text" name="year_level" value="<?php echo $row->year_level;?>">
										</div>
										<label>Semester</label>
										<input id="semester-<?php echo $row->stud_grades_no;?>" type="text" name="semester" value="<?php echo $row->semester;?>">

										<label>Instructor</label>
										<input id="instructor-<?php echo $row->stud_grades_no;?>" type="text" name="instructor" value="<?php echo $row->instructor;?>">

										<label>Subject Code</label>
										<input type="text" name="subject_code" value="<?php echo $row->subject_code;?>">

										<input type="text" name="subject_title" value="<?php echo $row->subject_title;?>">
										<input type="text" name="units" value="<?php echo $row->no_units;?>">
										<input type="text" name="prelim" value="<?php echo $row->prelim;?>">
										<input type="text" name="midterm" value="<?php echo $row->midterm;?>">
										<input type="text" name="prefinal" value="<?php echo $row->prefinal;?>">
										<input type="text" name="final" value="<?php echo $row->final;?>">
										<input type="text" name="rating" value="<?php echo $row->rating;?>">
									</div>
									<div class="modal-footer">
										<!--<a href="#" class="btn" data-dismiss="modal">Close</a>-->
										<button onclick="updateData(<?php echo $row->stud_grades_no;?>)" type="submit" style="width: 150px; font-size: 16px;padding: 10px;" class="btn-design">Edit This Subject</button>
									</div>
									</form>
								</div>
						<?php } ?>
						<script type="text/javascript">
							function updateData(<?php echo $row->stud_grades_no;?>)
							{
								/*var stud_grades_no = $('#stud_grades_no-'+str).val();;
								var student_id = $('#student_id-'+str).val();
								var course = $('#course-'+str).val();
								var year_level = $('#year_level-'+str).val();
								var semester = $('#semester-'+str).val();

								$.ajax({
									type: "POST",
									
									url: "registrar/update_subModal/?stud_grades_no=" +str,
									data: "student_id="+student_id+"&course="+course+"&year_level="+year_level+"&semester="+semester+"&stud_grades_no="+stud_grades_no,
									success:function(data)
									{
										window.alert("Hello!");
									}
								});*/
								
								$.ajax({
									url:"<?=base_url()?>registrar/update_subModal/?stud_grades_no=" +<?php echo $row->stud_grades_no;?>,
									type:"GET",
									dataType: "JSON",
									success: function(data)
									{
										$('[name="stud_grades_no"]').val(data.stud_grades_no);
										$('[name="student_id"]').val(data.student_id);
										$('[name="course"]').val(data.course);
										$('[name="year_level"]').val(data.year_level);
										$('[name="semester"]').val(data.semester);
									},
									errors: function (jqXHR , textStatus , errorThrown)
									{
										alert('error get data from ajax');
									}
								});



							}
						</script>
					<!-- 
					Edit Subject
					-->
		</div>
       

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<!-- Change Password -->
	<div class="modal hide fade" id="change_pass">
	<form action="<?=base_url()?>registrar/change_password" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<table>
					<tr>
						<td>
							Do you want to change your Password?
						</td>
					</tr>
				</table>
				<table>

					<tr hidden="">
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="registrar_id" value="<?php echo $registrar_id;?>">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="password" name="old_pass" placeholder="Enter Old Password">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="new_pass" placeholder="Enter New Password">
						</td>
					</tr>
				</table>
								
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">No</a>
			<button href="#" class="btn btn-primary" type="submit">Yes</button>
		</div>
	</form>
	</div>
	<!-- Change Password -->
	<div class="modal hide fade" id="profile_to">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Profile</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<style type="text/css">
					.info_to
					{
						font-family: century gothic;
						text-align: center;
					}
				</style>
				<table class="info_to">
					<tr><pre>Registrar ID: <?php echo $registrar_id;?></pre></tr>
					<tr><pre>First Name:   <?php echo $firstname;?></pre></tr>
					<tr><pre>Middle Name:  <?php echo $middlename;?></pre></tr>
					<tr><pre>Last Name:    <?php echo $lastname;?></pre></tr>
					<tr><pre>Gender:       <?php echo $gender;?></pre></tr>
					<tr><pre>Username:     <?php echo $username;?></pre></tr>
				</table>			
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<!--<a href="#" class="btn btn-primary">Save changes</a>-->
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
