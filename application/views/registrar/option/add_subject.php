<!DOCTYPE html>
<html lang="en">
<?php
if (isset($this->session->userdata['is_logged_in'])) {
$registrar_id = ($this->session->userdata['is_logged_in']['registrar_id']);

$lastname = ($this->session->userdata['is_logged_in']['lastname']);
$firstname = ($this->session->userdata['is_logged_in']['firstname']);
$middlename = ($this->session->userdata['is_logged_in']['middlename']);
$gender = ($this->session->userdata['is_logged_in']['gender']);
$emailaddress= ($this->session->userdata['is_logged_in']['emailaddress']);
$username = ($this->session->userdata['is_logged_in']['username']);
$password = ($this->session->userdata['is_logged_in']['password']);


} else {
header("location: login");
}
?>






<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Add Subject | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->

	<link rel="shortcut icon" href="<?=base_url()?>assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/style.css">
	<style type="text/css">
		
		.controls {
			border-bottom: 1px solid #e6e6e6;
			text-align: center;
			margin-left: 60px;
			color: #595959;

		}

		.control-label {
			font-size: 18px;
			color: #008000;
			margin-top: 25px;
			margin-left: 60px;
		}
		.avatar {
			border-radius: 50%;
		}
		
		.hideto
		{
			display: none;
		}
		#panel, #flip {
		    padding: 5px;
		    text-align: center;
		    background-color: #e5eecc;
		    border: solid 1px #c3c3c3;
		}

		#panel {
		    padding: 3px;
		    display: none;
		}
		#flip
		{
			cursor: pointer;
		}

		
	</style>
		
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="http://localhost/Student-Portal/registrar/studentlist"><img src="<?=base_url()?>assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span class="design-for">Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Admin <?php echo $username;?> 
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="<?=base_url()?>assets/logo.png" width="100px"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting" data-target="#change_pass" data-toggle="modal"><i class="halflings-icon lock"></i> Change Password</a></li>
								<li><a href="#" class="btn-setting" data-target="#profile_to" data-toggle="modal"><i class="halflings-icon user"></i> Profile Info</a></li>

								<li><a href="http://localhost/Student-Portal/registrar/logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="http://localhost/Student-Portal/registrar/studentlist"><i class="icon-credit-card"></i><span class="hidden-tablet"> Student List</span></a></li>	

						<li><a href="http://localhost/Student-Portal/registrar/addstudent"><i class="icon-user"></i><span class="hidden-tablet"> Add Student</span></a></li>	
						
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="http://localhost/Student-Portal/registrar/studentlist">Student List</a></li>
				<li><i class="icon-angle-right"></i></li>
				<li><a href="#"><?php foreach($idto->result() as $row) {  echo $row->student_id; }?> Subjects</a></li>
			</ul>
			<div class="row-fluid">





					<div class="border-pers" style="width: 98%;">

							<!-- Start here -->
					<div class="box-header" data-original-title>
						<h2><i class="icon-file-alt"></i><span class="break"></span>Subject List</h2>
						<div class="box-icon">
							<!--<a href="#flip" class="" title="Add Subject"><i class="halflings-icon plus-sign"></i></a>
							<a href="<?=base_url()?>registrar/download_registrar_sub/?student_id=<?php echo $row->student_id;?>" class="" title="Download Subjects"><i class="halflings-icon download-alt"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
<script src="<?=base_url()?>assets/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("fast");
    });
    
});
</script>
 

<div>
	<div id="flip">
		Click here to add subject.
	</div>
	<div id="panel">
						<form action="<?=base_url();?>registrar/insert_subject" method="post">
							<div>
								<table>
								<?php foreach($idto->result() as $row) { ?>
									<td hidden=""><input class="input_sub" type="text" name="student_id" placeholder="Student ID" value="<?php echo $row->student_id;?>"></td>
									<td hidden=""><input class="input_sub" type="text" name="course" placeholder="Course" value="<?php echo $row->course;?>"></td>
								<?php }?>
									<td>
									<select style="width: 100px;" id="speci" class="input_sub" name="year_level" required="">
										<option selected="" hidden="" disabled="">Year Level</option>
										<option>1st</option>
										<option>2nd</option>
										<option>3rd</option>
										<option>4th</option>
									</select>
									</td>
									<!--<td><input id="speci" class="input_sub" type="text" name="year_level" placeholder="Year Level" required=""></td>-->
									<td>
									<select style="width: 100px;" id="speci" class="input_sub" name="semester" required="">
										<option selected="" hidden="" disabled="">Semester</option>
										<option>1st</option>
										<option>2nd</option>
									</select>
									</td>
									<!--<td><input id="speci" class="input_sub" type="text" name="semester" placeholder="Semester" required=""></td>-->
									<!--<td><input class="input_sub" type="text" name="instructor" placeholder="Instructor" required=""></td>-->
									<td><input id="spec" class="input_sub" type="text" name="subject_code" placeholder="Code" required=""></td>
									<td><input style="width: 250px;" class="input_sub" type="text" name="subject_title" placeholder="Subject Title" required=""></td>
									<td><input id="spec" class="input_sub" type="text" name="no_units" placeholder="Units" required=""></td>
									<!--<td><input id="speci" class="input_sub" type="text" name="prelim" placeholder="Prelim"></td>
									<td><input id="speci" class="input_sub" type="text" name="midterm" placeholder="Midterm"></td>
									<td><input id="speci" class="input_sub" type="text" name="prefinal" placeholder="Pre-final"></td>
									<td><input id="speci" class="input_sub" type="text" name="final" placeholder="Final"></td>
									<td><input id="speci" class="input_sub" type="text" name="rating" placeholder="Rating"></td>-->
								</tr>
								</table>
								<table>
									<tr>
										<td><button class="button_sub" type="submit"> Save Subject </button></td>
									</tr>
								
								</table>
							</div>
						</form>
	</div>
</div>	
					<div class="box-content"  style="z-index:0 !important;">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  <th class="tbl_header" title="Sort Year Level">Year Level</th>
								  <th class="tbl_header" title="Sort Semester">Semester</th>
								  <th class="tbl_header" title="Sort Instructor">Instructor</th>
								  <th class="tbl_header" title="Sort Code">Code</th>
								  <th class="tbl_header" title="Sort Description">Description</th>
								  <th class="tbl_header" title="Sort No. Units">No. Units</th>
								  <th class="tbl_header" title="Sort Prelim">Prelim</th>
								  <th class="tbl_header" title="SortMidterm">Midterm</th>
								  <th class="tbl_header" title="Sort Pre-final">Pre-final</th>
								  <th class="tbl_header" title="Sort Final">Final</th>
								  <th class="tbl_header" title="Sort Rating">Rating</th>
							  </tr>
						  </thead>   
						  <tbody>
						  <!-- <?php ?>-->

						 <?php foreach($subjects->result() as $row) {?>
							<tr>
								<td class="center"><?php echo $row->year_level;?></td>
								<td class="center"><?php echo $row->semester;?></td>
								<td class="center"><?php echo $row->instructor;?></td>
								<td class="center"><?php echo $row->subject_code;?></td>
								<td class="center"><?php echo $row->subject_title;?></td>
								<td class="center"><?php echo $row->no_units;?></td>
								<td class="center"><?php echo $row->prelim;?></td>
								<td class="center"><?php echo $row->midterm;?></td>
								<td class="center"><?php echo $row->prefinal;?></td>
								<td class="center"><?php echo $row->final;?></td>
								<td class="center"><?php echo $row->rating;?></td>
							</tr>
						<?php } ?>
						
						  </tbody>
					  </table>            
					</div>
					</div>

					<div class="break_line"></div>

					<div class="border-pers" style="width: 98%;">
					<h4>Recommended Subject</h4>
					<table style="width: 100%;">
						
						  <thead>
							  <tr>
							  	  <th class="tbl_header" title="Sort Year Level">Course</th>
								  <th class="tbl_header" title="Sort Semester">Year Level</th>
								  <th class="tbl_header" title="Sort Instructor">Semester</th>
								  <th class="tbl_header" title="Sort Code">Subject Code</th>
								  <th class="tbl_header" title="Sort Description">Subject Title</th>
								  <th class="tbl_header" title="Sort No. Units">Subject Description</th>
								  <th class="tbl_header" title="Sort Prelim">No. Units</th>
							  </tr>
						  </thead>   
						  <tbody>
						  <!-- <?php ?>-->
						<?php foreach($subjects_demo->result() as $row) { ?>
							<tr class="tr_sub">
								<td class="td_sub"><?php echo $row->course;?></td>
								<td class="td_sub"><?php echo $row->year_level;?></td>
								<td class="td_sub"><?php echo $row->semester;?></td>
								<td class="td_sub"><?php echo $row->subject_code;?></td>
								<td class="td_sub"><?php echo $row->subject_title;?></td>
								<td class="td_sub"><?php echo $row->subject_description;?></td>
								<td class="td_sub"><?php echo $row->no_units;?></td>
							</tr>
						<?php } ?>
						
						  </tbody>
					  </table>

					  <div class="sub_break"></div>

					  </div>

					</div>



			</div>
			
		</div>
       

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		


	<!-- Change Password -->
	<div class="modal hide fade" id="change_pass">
	<form action="<?=base_url()?>registrar/change_password" method="post">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<table>
					<tr>
						<td>
							Do you want to change your Password?
						</td>
					</tr>
				</table>
				<table>

					<tr hidden="">
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="registrar_id" value="<?php echo $registrar_id;?>">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="password" name="old_pass" placeholder="Enter Old Password">
						</td>
					</tr>
					<tr>
						<td>
							<input style="width: 150px;font-size: 15px !important;" type="text" name="new_pass" placeholder="Enter New Password">
						</td>
					</tr>
				</table>
								
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">No</a>
			<button href="#" class="btn btn-primary" type="submit">Yes</button>
		</div>
	</form>
	</div>
	<!-- Change Password -->
	<div class="modal hide fade" id="profile_to">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Profile</h3>
		</div>
		<div class="modal-body">
			
			<div class="controls">
				<style type="text/css">
					.info_to
					{
						font-family: century gothic;
						text-align: center;
					}
				</style>
				<table class="info_to">
					<tr><pre>Registrar ID: <?php echo $registrar_id;?></pre></tr>
					<tr><pre>First Name:   <?php echo $firstname;?></pre></tr>
					<tr><pre>Middle Name:  <?php echo $middlename;?></pre></tr>
					<tr><pre>Last Name:    <?php echo $lastname;?></pre></tr>
					<tr><pre>Gender:       <?php echo $gender;?></pre></tr>
					<tr><pre>Username:     <?php echo $username;?></pre></tr>
				</table>			
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<!--<a href="#" class="btn btn-primary">Save changes</a>-->
		</div>
	</div>
	
	<div class="clearfix"></div>


	<footer>

		<p>
			<br>
			<span style="text-align:center;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="<?=base_url()?>assets/spi_logo_transparent_v3.png" width="300"><br>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="<?=base_url()?>assets/js/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?=base_url()?>assets/js/modernizr.js"></script>
	
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
	
		<script src="<?=base_url()?>assets/js/fullcalendar.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>

		<script src="<?=base_url()?>assets/js/excanvas.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.pie.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.stack.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.chosen.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uniform.min.js"></script>
		
		<script src="<?=base_url()?>assets/js/jquery.cleditor.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.elfinder.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.gritter.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.imagesloaded.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.masonry.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.knob.modified.js"></script>
	
		<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js"></script>
	
		<script src="<?=base_url()?>assets/js/counter.js"></script>
	
		<script src="<?=base_url()?>assets/js/retina.js"></script>

		<script src="<?=base_url()?>assets/js/custom.js"></script>

		<!--<script src="<?=base_url()?>assets/jquery.min.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
