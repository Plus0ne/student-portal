<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title> Notification | Skill-Power Institute</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="../assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="../assets/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="../assets/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="../assets/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="../assets/spilogo-enhanced.png" type="image/x-icon" />
	<!-- end: Favicon -->
	<link rel="stylesheet" type="text/css" href="../assets/style.css">
		<style type="text/css">
			body {
			font-family: century gothic;
		}
		.border-pers {
			width: 97%;
		}
		</style>
</head>

<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="http://localhost/SISwithCI/student/studentinfo"><img src="../assets/spilogo-enhanced.png" style="width:50px;height:50px;"><span>Skill-Power Institute</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						<li class="dropdown hidden-phone">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="icon-envelope"></i>
							</a>
							<ul class="dropdown-menu notifications">
								<li class="dropdown-menu-title">
 									<span>You have 11 notifications</span>
									<a href="#refresh"><i class="icon-repeat"></i></a>
								</li>	
                            	<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">1 min</span> 
                                    </a>
                                    
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">7 min</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">8 min</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">16 min</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">36 min</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">1 hour</span> 
                                    </a>
                                </li>
								<li class="warning">
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">2 hour</span> 
                                    </a>
                                </li>
								<li class="warning">
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">6 hour</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">yesterday</span> 
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="icon blue"><i class="icon-user"></i></span>
										<span class="message">New student registration</span>
										<span class="time">yesterday</span> 
                                    </a>
                                </li>
                                <li class="dropdown-menu-sub-footer">
                            		<a href="http://localhost/SISwithCI/registrar/studentnotif">View all notifications</a>
								</li>	
							</ul>
						</li>

						
						<!-- start: Message Dropdown -->
						<li class="dropdown hidden-phone">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="icon-comments"></i>
							</a>
							<ul class="dropdown-menu messages">
								<li class="dropdown-menu-title">
 									<span>You have 9 messages</span>
									<a href="#refresh"><i class="icon-repeat"></i></a>
								</li>	
                            	<li>
                                    <a href="#">
										<span class="avatar"><img src="../assets/img/avatar.jpg" alt="Avatar"></span>
										<span class="header">
											<span class="from">
										    	Dennis Ji
										     </span>
											<span class="time">
										    	6 min
										    </span>
										</span>
                                        <span class="message">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                        </span>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
										<span class="avatar"><img src="../assets/img/avatar.jpg" alt="Avatar"></span>
										<span class="header">
											<span class="from">
										    	Dennis Ji
										     </span>
											<span class="time">
										    	56 min
										    </span>
										</span>
                                        <span class="message">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                        </span>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
										<span class="avatar"><img src="../assets/img/avatar.jpg" alt="Avatar"></span>
										<span class="header">
											<span class="from">
										    	Dennis Ji
										     </span>
											<span class="time">
										    	3 hours
										    </span>
										</span>
                                        <span class="message">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                        </span>  
                                    </a>
                                </li>
								<li>
                                    <a href="#">
										<span class="avatar"><img src="../assets/img/avatar.jpg" alt="Avatar"></span>
										<span class="header">
											<span class="from">
										    	Dennis Ji
										     </span>
											<span class="time">
										    	yesterday
										    </span>
										</span>
                                        <span class="message">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                        </span>  
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
										<span class="avatar"><img src="../assets/img/avatar.jpg" alt="Avatar"></span>
										<span class="header">
											<span class="from">
										    	Dennis Ji
										     </span>
											<span class="time">
										    	Jul 25, 2012
										    </span>
										</span>
                                        <span class="message">
                                            Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                        </span>  
                                    </a>
                                </li>
								<li>
                            		<a onclick="document.getElementById('showthis').style.display='inline-block'" class="dropdown-menu-sub-footer" href="#">View all messages</a>
								</li>	
							</ul>
						</li>
						<!-- end: Message Dropdown -->
						<!--<li class="">
							<a class="btn" href="#">
								<i class="halflings-icon white wrench"></i>
							</a>

						</li>-->
						<!-- start: User Dropdown -->
						<li class="dropdown">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="halflings-icon white user"></i> Romel Cubelo
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li class="dropdown-menu-title">
									<center><img class="prof-pic" src="../assets/img/avatar.jpg"></center>
 									<span>Account Settings</span>
								</li>
								<li><a href="#" class="btn-setting"><i class="halflings-icon user"></i> Profile Picture</a></li>
								<li><a href="#" class="btn-setting"><i class="halflings-icon lock"></i> Change Password</a></li>

								<li><a href="logout"><i class="halflings-icon off"></i> Logout</a></li>
							</ul>
						</li>
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
				<div class="nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">

						<li><a href="http://localhost/SISwithCI/student/studentinfo"><i class="icon-credit-card"></i><span class="hidden-tablet"> My Information</span></a></li>	
						<!--<li><a href="http://localhost/SISwithCI/thesis/messages"><i class="icon-comments"></i><span class="hidden-tablet"> Messages</span></a></li>-->
						
						<li><a href="http://localhost/SISwithCI/student/curriculum"><i class="icon-book"></i><span class="hidden-tablet"> Curriculum</span></a></li>
						
						<li><a href="http://localhost/SISwithCI/student/studentnotif"><i class="icon-envelope"></i><span class="hidden-tablet"> Notification</span></a></li>
						
					</ul>
				</div>
			</div>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="http://localhost/SISwithCI/student/studentnotif">Notification</a></li>
			</ul>
			<div class="row-fluid">

							<!-- Start here -->
							<div class="border-pers">
							<div class="box span12">
					
					<div class="box-header">
						<h2><i class="halflings-icon bullhorn"></i><span class="break"></span>Notification</h2>
						<div class="box-icon">
							<!--<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>-->
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
					<div class="box-content alerts"  style="z-index: 0 !important;">
						<div class="alert alert-error">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Oh snap!</strong> No Classes this Monday
						</div>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Well done!</strong> No Classes this Tuesday
						</div>
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>Heads up!</strong> No Classes this Wednesday
						</div>
						<div class="alert alert-block ">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<h4 class="alert-heading">Warning!</h4>
							<p>Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
						</div>
					</div>
					
				</div>
				<!-- modal custom change the data target and id
				<button data-target="#setting" data-toggle="modal"> ok </button>

				<div class="modal hide fade" id="setting">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h3>Settings</h3>
					</div>
					<div class="modal-body">
					dadasdasdas
					</div>
					<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Close</a>
					<a href="#" class="btn btn-primary">Save changes</a>
					</div>
				</div>
				-->



							<!-- End Here -->
							<!-- Chat source code start-->
					<div class="box span4" ontablet="span6" ondesktop="span4" style="position:fixed;right:0px;bottom:-28px;border-left:1px solid #cccccc;border-top:1px solid #cccccc">
					<div class="box-header">
						<h2><i class="icon-comments-alt"></i><span class="break"></span>Chats</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-down"></i></a>
							<!--<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>-->
						</div>
					</div>
					<div id="showthis" style="display: none;" class="box-content">
						<ul class="chat">
							<li class="left">
								<img class="avatar" alt="Dennis Ji" src="../assets/img/avatar.jpg">
									<span class="message"><span class="arrow"></span>
									<span class="from">Dennis Ji</span>
									<span class="time">Jul 25, 2012 11:09</span>
									<span class="text">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
									</span>
								</span>	                                  
							</li>
							<li class="right">
								<img class="avatar" alt="Dennis Ji" src="../assets/img/avatar.jpg">
								<span class="message"><span class="arrow"></span>
									<span class="from">Dennis Ji</span>
									<span class="time">Jul 25, 2012 11:08</span>
									<span class="text">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
									</span>
								</span>                                  
							</li>
							
							
						</ul>
						<div class="chat-form">
							<textarea></textarea>
							<button class="btn btn-info">Send message</button>
						</div>	
					</div>
					<!-- Chat source code end-->
			
			</div>
			</div>
       </div>

		</div><!--/.fluid-container-->
		
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<center><img class="prof-pic" src="../assets/img/avatar.jpg"></center>
			<div class="controls">
								<div id="uniform-fileInput" class="uploader"><input class="input-file uniform_on" id="fileInput" type="file"><span style="-moz-user-select: none;" class="filename">No file selected</span><span style="-moz-user-select: none;" class="action">Choose File</span></div>
							  </div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<br>
			<span style="text-align:left;float:left">&copy; 2016 <a href="http://www.spi.edu.ph" alt="Skill-Power Institute">Skill-Power Institute Antipolo Branch</a></span>
			<br>
			<img src="../assets/spi_logo_transparent_v3.png" width="300">
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="../assets/js/jquery-1.9.1.min.js"></script>
	<script src="../assets/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="../assets/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="../assets/js/jquery.ui.touch-punch.js"></script>
	
		<script src="../assets/js/modernizr.js"></script>
	
		<script src="../assets/js/bootstrap.min.js"></script>
	
		<script src="../assets/js/jquery.cookie.js"></script>
	
		<script src="../assets/js/fullcalendar.min.js"></script>
	
		<script src="../assets/js/jquery.dataTables.min.js"></script>

		<script src="../assets/js/excanvas.js"></script>
	<script src="../assets/js/jquery.flot.js"></script>
	<script src="../assets/js/jquery.flot.pie.js"></script>
	<script src="../assets/js/jquery.flot.stack.js"></script>
	<script src="../assets/js/jquery.flot.resize.min.js"></script>
	
		<script src="../assets/js/jquery.chosen.min.js"></script>
	
		<script src="../assets/js/jquery.uniform.min.js"></script>
		
		<script src="../assets/js/jquery.cleditor.min.js"></script>
	
		<script src="../assets/js/jquery.noty.js"></script>
	
		<script src="../assets/js/jquery.elfinder.min.js"></script>
	
		<script src="../assets/js/jquery.raty.min.js"></script>
	
		<script src="../assets/js/jquery.iphone.toggle.js"></script>
	
		<script src="../assets/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="../assets/js/jquery.gritter.min.js"></script>
	
		<script src="../assets/js/jquery.imagesloaded.js"></script>
	
		<script src="../assets/js/jquery.masonry.min.js"></script>
	
		<script src="../assets/js/jquery.knob.modified.js"></script>
	
		<script src="../assets/js/jquery.sparkline.min.js"></script>
	
		<script src="../assets/js/counter.js"></script>
	
		<script src="../assets/js/retina.js"></script>

		<script src="../assets/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
