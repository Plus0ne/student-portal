<!DOCTYPE html>
<html>
<head>
	<title>
		Error
	</title>
	<style type="text/css">
		body
		{
			margin: 0px;
		}
		.body-content
		{
			width: 100%;
			height: 100%;
		}
		.head
		{
			background-color: #008000;
			font-size: 30px;
			color: white;
		}
		.body
		{
			width: 100%;
			height: 362px;
			background-color: white;
		}
		.footer
		{
			/*background-color: #008000;*/
			position: fixed;
			bottom: 0px;
			width: 100%;
		}
		.head , .footer
		{
			padding:  10px;
		}
		p
		{
			margin-left: 10%;
			margin-right: 10%;
		}
		h2
		{
			margin-left: 10%;
			margin-right: 10%;
		}
		h3
		{
			margin-left: 10%;
			margin-right: 10%;
			color: #d60202;
		}
		a
		{
			width: 100px;
			text-decoration: none;
			color: white;
			
		}
		.btn
		{
			background-color: #008000;
			padding: 10px;
			width: 150px;
			text-align: center;
			font-size: 18px;
		}
		.btn:hover
		{
			background-color: #00a000;
		}
	</style>
</head>
<body>
<div class="body-content">
	<div class="head">
		<h2><?=$header;?></h2>
	</div>
	<div class="body">
		<h3><?=$message;?></h3>
	</div>
	<div class="footer">
		<a href="<?=base_url()?>Registrar/addstudent">
			<div class="btn">
				Return
			</div>
		</a>
	</div>
</div>


</body>
</html>