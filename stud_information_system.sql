-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2016 at 10:57 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stud_information_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `picture_id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fullpath` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`picture_id`, `student_id`, `filename`, `fullpath`) VALUES
(1, '15BS-0250', 'WIN_20150907_150419.JPG', 'C:/xampp/htdocs/Student-Portal/upload/WIN_20150907_150419.JPG'),
(4, '15BS-0251', 'Kurumi_School2.png', 'C:/xampp/htdocs/Student-Portal/upload/Kurumi_School2.png'),
(5, '16-BS-001', '', ''),
(6, '15BS-0250', 'WIN_20150907_150419.JPG', 'C:/xampp/htdocs/Student-Portal/upload/WIN_20150907_150419.JPG'),
(7, '15BS-0250', 'WIN_20150907_150419.JPG', 'C:/xampp/htdocs/Student-Portal/upload/WIN_20150907_150419.JPG'),
(8, '15BS-0253', '', ''),
(9, '15BS-0269', '', ''),
(10, '141222', '', ''),
(11, '454545', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `registrar`
--

CREATE TABLE `registrar` (
  `registrar_no` int(11) NOT NULL,
  `registrar_id` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrar`
--

INSERT INTO `registrar` (`registrar_no`, `registrar_id`, `lastname`, `firstname`, `middlename`, `gender`, `email_address`, `username`, `password`) VALUES
(1, '16REG-001', 'Dalaguit', 'Ernan', 'S', 'Male', 'ernan_dalaguit@gmail.com', 'Renz', '123456'),
(2, '16REG-002', 'Santos', 'Angelyn', 'E', 'Female', 'dikitanan@gmail.com', 'Ange', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_number` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_password` varchar(255) NOT NULL,
  `student_status` varchar(255) NOT NULL,
  `sur_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `home_address` varchar(255) NOT NULL,
  `tel_no` varchar(255) NOT NULL,
  `cell_no` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `major_in` varchar(255) NOT NULL,
  `school_year` varchar(255) NOT NULL,
  `year_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_number`, `student_id`, `student_password`, `student_status`, `sur_name`, `first_name`, `middle_name`, `gender`, `age`, `date_of_birth`, `status`, `citizenship`, `home_address`, `tel_no`, `cell_no`, `email_address`, `course`, `major_in`, `school_year`, `year_level`, `semester`) VALUES
(1, '15BS-0250', '654321', 'Transfered', 'Cubelo', 'Romel', 'P', 'Male', 21, 'June-19-1995', 'Single', 'Filipino', 'East Villa Road Parugan 3 Brgy. San Jose', '9876511', '214748364712', 'britzjam@gmail.com', 'Bachelor of Science in Business Adminstration', 'Programming', '2016/2017', '3rd Year', '2nd Semester');

-- --------------------------------------------------------

--
-- Table structure for table `student_grades`
--

CREATE TABLE `student_grades` (
  `stud_grades_no` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `year_taken` varchar(255) NOT NULL,
  `year_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `instructor` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `subject_title` varchar(255) NOT NULL,
  `no_units` varchar(255) NOT NULL,
  `prelim` varchar(255) NOT NULL,
  `midterm` varchar(255) NOT NULL,
  `prefinal` varchar(255) NOT NULL,
  `final` varchar(255) NOT NULL,
  `rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_grades`
--

INSERT INTO `student_grades` (`stud_grades_no`, `student_id`, `course`, `year_taken`, `year_level`, `semester`, `instructor`, `subject_code`, `subject_title`, `no_units`, `prelim`, `midterm`, `prefinal`, `final`, `rating`) VALUES
(18, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '3rd', '1st', 'Mr. Dalaguit', 'SE', 'Software Engineering', '3', '3', '', '', '', ''),
(19, '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(20, '15BS-0250', 'asd', '', '2nd', '1st', 'Mr. Denmark Secormayle', 'WD', 'Web Design', '3', '', '', '', '', ''),
(22, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '1st', '2nd', 'Ms. Yap', 'CA', 'College Algebra', '3', '', '', '', '', ''),
(23, '15BS-0251', 'Bachelor of Science in Computer Science', '', '1st', '1st', 'Romel', 'CA', 'College Algebra', '2', '', '1.0', '1.0', '1.0', '1.0'),
(24, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '4th', '1st', 'Mr. Basista', 'TH', 'THESIS', '3', '', '', '', '', ''),
(25, '15BS-0251', 'Bachelor of Science in Computer Science', '', '1st', '2nd', 'asd', 'dda', 'das', '2', '', '', '', '', ''),
(26, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '4th', '1st', 'Mr. Denmark Secormayle', 'AI', 'Artificial Intelligence', '3', '', '', '', '', ''),
(27, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '4th', '2nd', '', 'OJT', 'Practicum', '3', '', '', '', '', ''),
(28, '15BS-0250', 'Bachelor of Science in Business Adminstration', '', '4th', '1st', '', 'AA', 'Art Appreciation', '3', '', '', '', '', ''),
(29, '15BS-0250', 'Bachelor of Science in Business Adminstration', '2012', '2nd', '2nd', 'Mr. Denmark Secormayle', 'OOP3', 'Object-Oriented Programming 3', '3', '', '', '', '', ''),
(30, '15BS-0250', 'Bachelor of Science in Business Adminstration', '2010', '1st', '2nd', '', 'DMATH', 'Discrete Mathematics', '3', '', '', '', '', 'Dropped'),
(31, '15BS-0269', 'Bachelor of Science in Computer Science', '2012', '2nd', '1st', '', 'CA', 'ccaca', '3', '', '', '', '', ''),
(32, '15BS-0269', 'Bachelor of Science in Computer Science', '2016', '2nd', '2nd', '', 'aaaa', 'aaaaa', '2', '', '', '', '', ''),
(33, '15BS-0250', 'Bachelor of Science in Business Adminstration', '2222', '1st', '1st', 'Mr. Denmark Secormayle', 'OOP1', 'Object-Oriented Programming', '3', '', '', '', '', ''),
(34, '15BS-0250', 'Bachelor of Science in Business Adminstration', '22', '2nd', '2nd', 'Ms. Yap', 'Trigo', 'Trigonometry', '3', '', '', '', '', ''),
(35, '15BS-0250', 'Bachelor of Science in Business Adminstration', '2012', '1st', '2nd', 'Mr. Denmark Secormayle', 'OOP2', 'Object-Oriented Programming 2', '3', '', '', '', '', 'Dropped'),
(36, '15BS-0250', 'Bachelor of Science in Business Adminstration', 'asd', '4th', '1st', '', 'adasdasd', 'asdsadasdas', 'dasdas', '', '', '', '', ''),
(37, '141222', 'Bachelor of Science in Computer Science', '2016', '1st', '1st', 'Mr. Cubelo', 'CA', 'College Algebra', '3', '3', '', '', '', 'Dropped');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_no` int(11) NOT NULL,
  `course` varchar(255) NOT NULL,
  `year_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `subject_title` varchar(255) NOT NULL,
  `subject_description` varchar(255) NOT NULL,
  `no_units` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects_demo`
--

CREATE TABLE `subjects_demo` (
  `subject_no` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `year_level` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `subject_title` varchar(255) NOT NULL,
  `subject_description` varchar(255) NOT NULL,
  `no_units` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects_demo`
--

INSERT INTO `subjects_demo` (`subject_no`, `course`, `year_level`, `semester`, `subject_code`, `subject_title`, `subject_description`, `no_units`) VALUES
('01', 'Bachelor of Science in Business Adminstration', '1st', '2nd', 'CA', 'College Algebra', 'College Algebra', 3),
('02', 'Bachelor of Science in Business Adminstration', '1st', '2nd', 'DADA', 'asdad', 'asdaCd', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`picture_id`);

--
-- Indexes for table `registrar`
--
ALTER TABLE `registrar`
  ADD PRIMARY KEY (`registrar_no`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`),
  ADD UNIQUE KEY `student_id` (`student_number`);

--
-- Indexes for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD PRIMARY KEY (`stud_grades_no`),
  ADD UNIQUE KEY `stud_grades_no` (`stud_grades_no`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_no`);

--
-- Indexes for table `subjects_demo`
--
ALTER TABLE `subjects_demo`
  ADD UNIQUE KEY `subject_no` (`subject_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `picture_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `registrar`
--
ALTER TABLE `registrar`
  MODIFY `registrar_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `student_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `student_grades`
--
ALTER TABLE `student_grades`
  MODIFY `stud_grades_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
